﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.EventSystems;
//using ChartboostSDK;
using UnityEngine.Advertisements;
using GoogleMobileAds.Api;

public enum BANNER_TYPE
{
	TOP_BANNER,
	BOTTOM_BANNER
}

public enum ADS_TYPE
{
	CROSS_PROMOTION,
	NETWORK
}

public class ZAdsMgr : ZSingleton<ZAdsMgr> {

	int m_currentTries;
	public int removeAds;
	BannerView bannerView;
	InterstitialAd interstitial;

	bool isInterstitialRequest;
	bool m_isCrossPromotionAvailable;

	void Start()
	{
		//Chartboost.didCompleteRewardedVideo += DidCompleteRewardedVideo;

		m_currentTries = 0;
		removeAds = PlayerPrefs.GetInt ("Removeads");
		isInterstitialRequest = false;
		m_isCrossPromotionAvailable = true;
		//RequestInterstitial ();

		//RequestInterstitial ();
	}

	public void ShowAdPopup()
	{	
		if (removeAds > 0)
			return;
		
		//if( isRateUs > 0 )
		//	return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog("Make Support the Dev!","Click yes to help the dev make more games!");
		ratePopUp.OnComplete += OnRatePopUpClose;

		//isRateUs++;
	}
	private void OnRatePopUpClose(MNDialogResult result) {
		if ( Advertisement.IsReady ()) {
			Advertisement.Show ("rewardedVideo");
		}
	}

	bool DontShowAd()
	{
		m_currentTries++;
		if (m_currentTries > 3) 
		{	m_currentTries = 0;
			return false;
		}
		else
		{	return true;
		}
	}

	public void ShowBanner(BANNER_TYPE type)
	{
		if (removeAds > 0)
			return;
		
		#if UNITY_ANDROID
		string adUnitId = ZGameMgr.instance.BANNER_URL_ANDROID;
		#elif UNITY_IPHONE
		string adUnitId = ZGameMgr.instance.BANNER_URL_IOS;
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Create a 320x50 banner at the top of the screen.
		bannerView = new BannerView(adUnitId, AdSize.SmartBanner, AdPosition.Bottom);


		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().Build();
		// Load the banner with the request.
		bannerView.LoadAd(request);
	}

	public void HideBannerAd()
	{
		bannerView.Destroy ();
	}

	public void RequestInterstitial()
	{
//		string testCode = (p_adsType == ADS_TYPE.NETWORK) ?
//		(ZGameMgr.instance.INTERSTITIAL_URL_ANDROID) : 
//		(ZGameMgr.instance.CROSS_PROMO_ADS_ANDROID);
//
//		Debug.Log ("testCode: " + testCode);

		if (removeAds > 0)
			return;

		if (isInterstitialRequest)
			return;



		#if UNITY_ANDROID
		string adUnitId = ZGameMgr.instance.INTERSTITIAL_URL_ANDROID;
		#elif UNITY_IPHONE
		string adUnitId = ZGameMgr.instance.INTERSTITIAL_URL_IOS;
		#else
		string adUnitId = "unexpected_platform";
		#endif

		// Initialize an InterstitialAd.
		interstitial = new InterstitialAd(adUnitId);
		// Create an empty ad request.
		AdRequest request = new AdRequest.Builder().AddTestDevice("2195C7E054D6754EAA79E9F66B0608BD").AddTestDevice("037218fed44ae9281af7ec409baba58a82c5f8e7").AddTestDevice(AdRequest.TestDeviceSimulator).Build();
		// Load the interstitial with the request.
		interstitial.LoadAd(request);

		isInterstitialRequest = true;

		Debug.Log ("Googlez : Request Interstitial");
	}

	public bool IsCrossPromotionAvailable()
	{
		if (removeAds > 0 || !m_isCrossPromotionAvailable)
			return false;
		else if (interstitial.IsLoaded ())
			return true;
		else {
			//RequestInterstitial (ADS_TYPE.CROSS_PROMOTION);
			Debug.Log ("Googlez : No Ad Availalbe");
		return false;
		}
	}

	public bool IsInterstitialAvailable()
	{
		if (removeAds > 0)
			return false;
		else if (interstitial.IsLoaded ())
			return true;
		else {
			//RequestInterstitial (ADS_TYPE.NETWORK);
			Debug.Log ("Googlez : No Ad Availalbe");
			return false;
		}
	}

	public void ShowCrossPromotion()
	{
		if (removeAds > 0)
			return;

		if ( IsCrossPromotionAvailable() ) 
		{
			interstitial.Show ();
			isInterstitialRequest = false;
			m_isCrossPromotionAvailable = false;
			//RequestInterstitial (ADS_TYPE.NETWORK);
		} 
	}

	public void ShowInsterstitial()
	{
		if (removeAds > 0)
			return;

		if ( IsInterstitialAvailable() ) 
		{
			interstitial.Show ();
			isInterstitialRequest = false;
			RequestInterstitial ();
			Debug.Log ("Googlez : Interstititial Available Show Now");
		} 


	}

	public void ShowVideoAd()
	{
		if( DontShowAd () )
			return;

		///Chartboost.showInterstitial(CBLocation.LevelComplete);
	}

	public void ShowRewardedVideoAds()
	{
		if( DontShowAd () )
			return;

		//Chartboost.showRewardedVideo(CBLocation.Default);
	}

	public void RequestRewardedVideoAd()
	{
		//Chartboost.cacheRewardedVideo(CBLocation.Default);
	}

	public bool HasRewardedVideoAd()
	{
		return false;
		//return Chartboost.hasRewardedVideo (CBLocation.Default);
	}

	//public void DidCompleteRewardedVideo(CBLocation location, int reward) {
	//	didCompleteRewardedVideo ();
	//}

	public static event Action didCompleteRewardedVideo;
}
