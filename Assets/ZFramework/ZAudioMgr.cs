﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZAudioMgr : ZSingleton<ZAudioMgr> {

	List<AudioSource> m_listAudioSource;

	void Awake()
	{
		InstantiateAudioSources ();
	}

	void InstantiateAudioSources()
	{
		m_listAudioSource = new List<AudioSource> ();

		for(int x = 0; x < 16; x++) {
			m_listAudioSource.Add(this.gameObject.AddComponent<AudioSource>());
		}
	}

	AudioSource GetAvailableAudioSource()
	{
		foreach (AudioSource source in m_listAudioSource) {
			if (!source.isPlaying) {
				return source;
			}
		}
		return null;
	}

	public void StopSFX(AudioClip clip)
	{
		for(int x = 0; x < 16; x++) {
			if( m_listAudioSource[x].clip == clip )
			{	m_listAudioSource[x].Stop();
			}
		}
	}

	public void PlaySFX(AudioClip clip, bool looping, float pitch)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = pitch;
		source.loop = looping;
		source.clip = clip;
		source.Play ();
	}

	public void PlaySFX(AudioClip clip, bool looping)
	{
		AudioSource source = GetAvailableAudioSource ();
		
		source.loop = looping;
		source.clip = clip;
		source.Play ();
	}

	public void PlaySFX(AudioClip clip)
	{
		AudioSource source = GetAvailableAudioSource ();

		if (source) {
			source.loop = false;
			source.clip = clip;
			source.Play ();
		}
	}

	public void PlaySFX(AudioClip clip, float pitch)
	{
		AudioSource source = GetAvailableAudioSource ();

		source.pitch = pitch;
		source.loop = false;
		source.clip = clip;
		source.Play ();
	}
}
