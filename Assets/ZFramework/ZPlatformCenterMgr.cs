﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.EventSystems;

using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi;

using GoogleMobileAds.Api;

public class ZPlatformCenterMgr : ZSingleton<ZPlatformCenterMgr> {
	public void Login () {
		
		#if UNITY_ANDROID
		PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();

		PlayGamesPlatform.InitializeInstance(config);
		// recommended for debugging:
		PlayGamesPlatform.DebugLogEnabled = true;
		// Activate the Google Play Games platform
		PlayGamesPlatform.Activate();
		#endif // UNITY_ANDROID

		// Authenticate and register a ProcessAuthentication callback
		// This call needs to be made before we can proceed to other calls in the Social API s
		Social.localUser.Authenticate (ProcessAuthentication);
	}

	// This function gets called when Authenticate completes
	// Note that if the operation is successful, Social.localUser will contain data from the server.
	void ProcessAuthentication (bool success) {
		if (success) {
			Debug.Log ("Authenticated, checking achievements");

			// Request loaded achievements, and register a callback for processing them
			Social.LoadAchievements (ProcessLoadedAchievements);
		}
		else
			Debug.Log ("Failed to authenticate");
	}

	// This function gets called when the LoadAchievement call completes
	void ProcessLoadedAchievements (IAchievement[] achievements) {
		if (achievements.Length == 0)
			Debug.Log ("Error: no achievements found");
		else
			Debug.Log ("Got " + achievements.Length + " achievements");

		// You can also call into the functions like this
		/*Social.ReportProgress ("Achievement01", 100.0, function(result) {
			if (result)
				Debug.Log ("Successfully reported achievement progress");
			else
				Debug.Log ("Failed to report achievement");
		});*/
	}

	public void ShowLeaderboardUI(string id = "")
	{
		#if UNITY_ANDROID
		((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(id);
		#else
		Social.ShowLeaderboardUI();
		#endif // UNITY_ANDROID
	}

	public void PostScore(int score, string leaderboardName)
	{
		long scoreLong = score; // Gamecenter requires a long variable
		Social.ReportScore(scoreLong,leaderboardName,HighScoreCheck);

		Debug.Log ("Score : " + score + ", Name : " + leaderboardName);
	}

	public void OpenAchievements()
	{
		Social.ShowAchievementsUI();
	}

	public void PostUnlockAchievement(string id = "")
	{
		#if UNITY_ANDROID
		Social.ReportProgress(id, 100.0f, (bool success) => {
			//	// handle success or failure
		} );


		#endif // UNITY_ANDROID
	}

	public void PostIncrementAchievement(string id, int value)
	{
		#if UNITY_ANDROID
		//PlayGamesPlatform.Instance.IncrementAchievement(
		//	id, value, (bool success) => {
		// handle success or failure
		//	} );
		#endif // UNITY_ANDROID
	}

	static void HighScoreCheck(bool result) {
		if(result)
			Debug.Log("score submission successful");
		else
			Debug.Log("score submission failed");
	}
}
