﻿using UnityEngine;
using System.Collections;

public class ZJumper : MonoBehaviour {

	public float duration;
	public Vector3 positionChange;
	public LeanTweenType easeType;

	// Use this for initialization
	void Start () {
		Move ();
	}

	public void Move()
	{
		LeanTween.moveLocal (this.gameObject, this.transform.localPosition + positionChange, duration).setLoopPingPong().setEase(easeType);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
