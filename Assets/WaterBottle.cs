﻿using UnityEngine;
using System.Collections;

public class WaterBottle : MonoBehaviour {

	public static WaterBottle Instance;
	public int isFlipped = 0;

	public int isGrounded = 0;
	public int isScoreGrounded = 0;
	public AudioClip m_audioHit;

	public float m_angularVelocity;

	private bool isFullFlip = false;

	// Use this for initialization
	void Awake () {
		Instance = this;
	}

	private bool isNotMoving()
	{
	//	m_pos = this.transform.eulerAngles;
		m_angularVelocity = this.GetComponent<Rigidbody2D> ().angularVelocity;
		if (this.GetComponent<Rigidbody2D> ().angularVelocity < 0.5f
			&& this.GetComponent<Rigidbody2D> ().angularVelocity > -0.5f) 
		{
			return true;
		} 
		return false;
	}

	private bool isUpright()
	{
		if (this.transform.eulerAngles.z <= 1 || this.transform.eulerAngles.z >= 359) 
		{
			return true;
		}

		return false;
	}

	private void checkIfFullFlip()
	{
		if (this.transform.eulerAngles.z >= 150 && this.transform.eulerAngles.z <= 210) 
		{
			isFullFlip = true;
		}
	}

	// Update is called once per frame
	void Update () 
	{
//		float test = this.GetComponent<Rigidbody2D> ().angularVelocity;
		if (GameScene.instance.GetMode() == 13) 
		{
			checkIfFullFlip ();
		}


		if (isFlipped == 2) 
		{
//			if (isNotMoving ()) 
//			{
//				GameScene.instance.Die ();
//			}
			//if(this.GetComponent<Rigidbody2D>().angularVelocity
			//Debug.Log("Velocity : " + this.GetComponent<Rigidbody2D>().angularVelocity);
			//if (this.GetComponent<Rigidbody2D> ().angularVelocity < 10 &&
			//	this.GetComponent<Rigidbody2D> ().angularVelocity > -10) {
			if (GameScene.instance.GetMode () == 7) 
			{
				if (this.GetComponent<Rigidbody2D> ().angularVelocity < 5 &&
				    this.GetComponent<Rigidbody2D> ().angularVelocity > -5) {
					if (this.transform.localEulerAngles.z < 10 ||
					    this.transform.localEulerAngles.z > 350) {
						Land ();
					}
				}
			} 
			if (GameScene.instance.GetMode () == 11) {
				if (this.GetComponent<Rigidbody2D> ().angularVelocity < 5 &&
				    this.GetComponent<Rigidbody2D> ().angularVelocity > -5) {
					if (this.transform.localEulerAngles.z < 10 ||
					    this.transform.localEulerAngles.z > 350) {
						Land ();
					}
					if (this.transform.localEulerAngles.z < 190 &&
					    this.transform.localEulerAngles.z > 170) {
						ReverseLand ();
					}
				}
			} else if (GameScene.instance.GetMode () == 10) {
				if (this.transform.localEulerAngles.z < 20 ||
				    this.transform.localEulerAngles.z > 340) {
					Land ();
				}
			} else if (GameScene.instance.GetMode () == 12) {
				if (this.GetComponent<Rigidbody2D> ().angularVelocity < 100 &&
				    this.GetComponent<Rigidbody2D> ().angularVelocity > -100) {
					if (this.transform.localEulerAngles.z < 20 ||
					    this.transform.localEulerAngles.z > 340) {
						Land ();
					}
				}
			} else if (GameScene.instance.GetMode () == 13) 
			{

				if (isNotMoving ()) 
				{
					if (isUpright () && isFullFlip) 
					{
						Land ();
					} else {
						GameScene.instance.Die ();
						isFlipped = 3;
					}
				}
			}
			else 
			{
				if (GameScene.instance.GetLevel () > 40 && this.GetComponent<Rigidbody2D> ().angularVelocity < 5 &&
				   this.GetComponent<Rigidbody2D> ().angularVelocity > -5) {
					if (this.transform.localEulerAngles.z < 20 ||
					   this.transform.localEulerAngles.z > 340) {
						Land ();
					}
				} else if (GameScene.instance.GetLevel () > 7 && this.GetComponent<Rigidbody2D> ().angularVelocity < 10 &&
				         this.GetComponent<Rigidbody2D> ().angularVelocity > -10) {
					if (this.transform.localEulerAngles.z < 20 ||
					   this.transform.localEulerAngles.z > 340) {
						Land ();
					}
				} 
				else if (this.GetComponent<Rigidbody2D> ().angularVelocity < 60 &&
				          this.GetComponent<Rigidbody2D> ().angularVelocity > -60) {
					if (this.transform.localEulerAngles.z < 20 ||
					   this.transform.localEulerAngles.z > 340) {
						Land ();
					}
				}
			}
		}
//		else if (isFlipped == 4) 
//		{
//			InsideCase ();
//		}

	}

	public void Score()
	{
		GameScene.instance.Score (1);
		WaterBottle.Instance.Reset ();
	}

	public void CheckScore()
	{
		
	}

//	public void InsideCase()
//	{
//		if (GameScene.instance.m_eState == GAME_STATE.RESULTS)
//			return;
//
//		GameScene.instance.Score (1);
//		isFlipped = 3;
//		GameScene.instance.MoveCamera (this.transform);
//		this.GetComponent<Rigidbody2D> ().isKinematic = true;
//	}

	public void ReverseLand()
	{
		if (GameScene.instance.m_eState == GAME_STATE.RESULTS)
			return;

		Score ();
		this.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		this.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		LeanTween.rotateLocal(this.gameObject, new Vector3(0,0, 180), 0.2f);
	}

	public void Land()
	{
		if (GameScene.instance.m_eState == GAME_STATE.RESULTS)
			return;
		
		Score ();
		this.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		this.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		//this.transform.localEulerAngles = Vector3.zero;
		LeanTween.rotateLocal(this.gameObject, Vector3.zero, 0.2f);
		//this.GetComponent<Rigidbody2D> ().angularDrag = 200f;
		//Reset();
	}

	public void Flip()
	{
		isFlipped = 1;
		//this.GetComponent<Rigidbody2D> ().angularDrag = 0.5f;
		this.GetComponent<Rigidbody2D> ().isKinematic = false;
		this.GetComponent<BoxCollider2D> ().enabled = false;
		LeanTween.delayedCall (0.2f, EnableColliders);
	}

	void EnableColliders()
	{
		this.GetComponent<BoxCollider2D> ().enabled = true;
	}

	public void Reset()
	{
		this.GetComponent<Rigidbody2D> ().centerOfMass = new Vector2 (0, -0.5f);
		GameScene.instance.MoveCamera (this.transform);
		isFlipped = 0;
		//if( GameScene.instance
		//isScoreGrounded = 0
		//this.GetComponent<Rigidbody2D> ().angularDrag = 200f;
		//this.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		//this.transform.localEulerAngles = Vector3.zero;
		this.GetComponent<Rigidbody2D> ().isKinematic = true;
		this.GetComponent<Rigidbody2D> ().velocity = Vector2.zero;
		this.GetComponent<Rigidbody2D> ().angularVelocity = 0;
		isFullFlip = false;
		//m_setScore = true;
		Debug.Log ("Reset");
	}

	public void SetGravity(float gravityValue )
	{
		this.GetComponent<Rigidbody2D> ().gravityScale = gravityValue;
		if (gravityValue == 8f) 
		{
			this.GetComponent<Rigidbody2D> ().angularDrag = 1;
		} 
		else 
		{
			this.GetComponent<Rigidbody2D> ().angularDrag = 0.4f;
		}
	}

	public void SetAngularDrag(float value)
	{
		this.GetComponent<Rigidbody2D> ().angularDrag = value;
	}

	void OnCollisionEnter2D(Collision2D coll) {
		
		if (coll.gameObject.tag == "Die") 
		{
			GameScene.instance.Die ();
			isFlipped = 3;
		}
		else if (isFlipped == 1) 
		{
			//LeanTween.delayedCall (2f, GroundedRoutine);
			//Debug.Log("Water Bottle");
			if (coll.gameObject.GetComponent<PlatformObject> () )
			{	
				if (coll.gameObject.GetComponent<PlatformObject> ().currentNumber > GameScene.instance.GetLevel ()) 
				{
					if (coll.gameObject.GetComponent<PlatformObject> ().isMoving != MOVESTATES.STATIC) 
					{
						LeanTween.cancel (coll.gameObject);
					}
					isFlipped = 2;
					Debug.Log ("Flip 2");
					if (GameScene.instance.GetLevel () < 1000) 
					{
						this.GetComponent<Rigidbody2D> ().angularVelocity /= 2f;
						this.GetComponent<Rigidbody2D> ().velocity /= 2f;
					}
				} 
				else 
				{
					//isFlipped = 0;
					GameScene.instance.Die ();
					Debug.Log ("Flip 0");
				}
			}
		}	

		//ZAudioMgr.Instance.PlaySFX (m_audioHit);
	}

	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Coin") 
		{
			coll.gameObject.SetActive (false);
			GameScene.instance.AddCoins (1, true);
		} 
//		else if (coll.gameObject.tag == "PencilCase") 
//		{
//			Debug.Log ("InsideCase");
//			isFlipped = 4;
//			//LeanTween.delayedCall (0.5f, Land);
//		}
	}

	void GroundedRoutine()
	{
		if (isGrounded < 2) {
			//GameScene.instance.Score (1);
			//isFlipped = 0;
			Debug.Log ("Score");
			//GameScene.instance.SetupLevel ();
		} else {
			GameScene.instance.Die ();
		}
	}

}
