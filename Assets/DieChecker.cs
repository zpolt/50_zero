﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class DieChecker : MonoBehaviour
{
	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.GetComponent<MTEnemy>()) {
			coll.gameObject.SetActive(false);
		}
	}
}
