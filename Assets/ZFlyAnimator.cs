﻿using UnityEngine;
using System.Collections;

public enum FLYANIMTYPE
{
	FLYFROMLEFT,
	FLYFROMRIGHT,
	FLYFROMTOP,
	FLYFROMBOTTOM
}

public class ZFlyAnimator : MonoBehaviour {

	public RectTransform canvasRect;

	public LeanTweenType tweenType = LeanTweenType.easeInOutCubic;
	public FLYANIMTYPE flyAnimType = FLYANIMTYPE.FLYFROMTOP;

	public float duration = 1f;
	public float moveValue = 2f;

	Vector3 currentPosition;

	// Use this for initialization
	void OnEnable () {
		LeanTween.alpha (this.gameObject, 1, 2f);
		return;
		//currentPosition = Camera.main.ScreenToWorldPoint(this.transform.position);
		//currentPosition = RectTransformUtility.ScreenPointToWorldPointInRectangle(Camera.main, this.transform.localPosition);
		//RectTransformUtility.ScreenPointToWorldPointInRectangle(this.transform,
		//RectTransformUtility.



		//currentPosition = canvasRect.GetComponent<RectTransform>().TransformPoint(this.GetComponent<RectTransform>().position);

		RectTransformUtility.ScreenPointToWorldPointInRectangle (canvasRect, this.transform.position, Camera.main, out currentPosition);
		Debug.LogWarning ("Position : " + currentPosition);

		/*switch (flyAnimType) {
		case FLYANIMTYPE.FLYFROMLEFT: 
			this.transform.localPosition += new Vector3 (-moveValue, 0, 0);
			break;
		case FLYANIMTYPE.FLYFROMRIGHT: 
			this.transform.localPosition += new Vector3 (moveValue, 0, 0);
			break;
		case FLYANIMTYPE.FLYFROMBOTTOM: 
			this.transform.localPosition += new Vector3 (0, -moveValue, 0);
			break;
		case FLYANIMTYPE.FLYFROMTOP:
			this.transform.localPosition += new Vector3 (0, moveValue, 0);
			break;
		}*/

		LeanTween.move (this.gameObject, currentPosition, duration).setEase (tweenType);
	}


	
	// Update is called once per frame
	void Update () {
	
	}
}
