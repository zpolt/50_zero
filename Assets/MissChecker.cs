﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class MissChecker : MonoBehaviour
{
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Arrow") {
			coll.gameObject.SetActive (false);
			GameScene.instance.Die ();
		}
	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Arrow") {
			coll.gameObject.SetActive (false);
		}
	}
}
