﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

//using ChartboostSDK;
using UnityEngine.Advertisements;

public enum GAME_STATE
{
	START,
	IDLE,
	AIMING,
	SHOOT,
	RESULTS
}

public enum GAME_MODES
{
	NORMAL_MODE = 0,
	ENDLESS_MODE,
	IMPOSSIBLE_MODE,
	ZEN_MODE,
	SPACE_MODE,
	BOTTLECAP_MODE,
	FREE_MODE,
	CAPLEVEL_MODE,
	TOWER_MODE,
	CHALLENGE_MODE,
	CITY_MODE,
	MARKER_MODE
}

public enum CityPrefabs
{
	BIRD = 16,
	CAR,
	CAT,
	CLOWN,
	HOTDOG,
	LAMPOST,
	TABLE,
	PHONE_BOOTH,
	SCAFFOLDING,
	TRUCK,
	WOMAN,
	STOP_LIGHT,
	DOG,
	BUS,
	BENCH,
	TREE,
	HYDRANT,
	BALLOON
}

public enum WindDirection
{
	LEFT,
	RIGHT
}

[System.Serializable]
public class ShopBird
{
	public GameObject objectImage;
	public Image imageButtons;
	public GameObject objectPrice;
	//public Sprite spriteImage;
	//public RuntimeAnimatorController animator;
	public string m_textName;
}

[System.Serializable]
public class CityModeLevel
{
	public float x;
	public float y;
	public CityPrefabs obj;
	public float windIntensity;
	public WindDirection direction;
}

[System.Serializable]
public class LevelColors
{
	public Color background;
	public Color topbar;
	public Color ground;
}

// iTunes - https://itunes.apple.com/us/app/make-pana-blue-eagle/id1119208392?mt=8
// https://play.google.com/store/apps/details?id=com.mostplayed.archereagle
// itms-apps://itunes.apple.com/app/idYOUR_APP_ID
// from iTunes PH : https://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8
// itms - itms-apps://itunes.apple.com/ph/app/make-pana-blue-eagle/id1119208392?mt=8

public class GameScene : MonoBehaviour 
{
	public static GameScene instance;

	public GameObject m_rootSpawnLocationLeft;
	public GameObject m_rootSpawnLocationRight;

	public List<ShopBird> m_listShopBirds;
	public List<ShopBird> m_listShopBalls;

	public GameObject m_objectWatchVideo;


	//public GameObject m_objectCharHead;
	public GameObject m_objectParticle;
	public GameObject m_objectParticleBlood;
	public GameObject m_objectParticlePerfect;

	public GameObject m_prefabEnemy;

	public GameObject m_prefabArrow;
	public GameObject m_prefabBird;
	public GameObject m_prefabBirdBig;
	public GameObject m_prefabBall;

	public GameObject m_objectTarget;
	public GameObject m_objectMiss;
	public GameObject m_objectSuccess;
	public GameObject m_objectTutorial;
	public GameObject m_objectTutorialTitle;
	public GameObject m_objectTopBar;
	public GameObject m_objectHitParticle;
	public GameObject m_objectHitBow;
	public GameObject m_objectEarned;

	public GameObject m_objectWindManager;

	public GameObject m_objectTextTutorial;

	public GameObject m_objectRootArrows;
	//public GameObject m_objectArrowMock;
	//public GameObject m_objectTulong;
	public GameObject m_objectNoInternet;
	public GameObject m_objectReadyUnlock;

	public GameObject m_objectTrumpsHairHead;
	//public GameObject m_objectTrumpsHairBird;

	public GameObject m_objectHeadshot;
	public GameObject m_directionalLight;

	//public GameObject m_objectNormal;
	//public GameObject m_objectWin;

	//public GameObject m_objectSplash;

	public GameObject m_objectBottle;

	public List<GameObject> m_listButtons;

	public List<Color> m_cityCameraColor;
	public List<Color> m_cityLightColor;


	public List<LevelColors> m_listModeColor;

	public GameObject m_materialBackground;
	public Image m_imageTopBar;
	public GameObject m_materialGround;
	public GameObject m_materialGround2;
	public Image m_imageBackground;

	public GameObject m_objectFloorDieObject;

	public AudioClip m_audioShoot;
	public AudioClip m_audioDie;
	public AudioClip m_audioMiss;
	public AudioClip m_audioMiss1;
	public AudioClip m_audioMiss2;
	public AudioClip m_audioMiss3;
	public AudioClip m_audioMiss4;
	public AudioClip m_audioMiss5;
	public AudioClip m_audioSuccess;
	public AudioClip m_audioBow;
	public AudioClip m_audioButton;
	public AudioClip m_audioHeadshot;
	public AudioClip m_audioCatch;
	public AudioClip m_audioBuy;
	public AudioClip m_audioScore;
	public AudioClip m_audioCoin;

	public AudioClip m_audioThrow;

	public Color m_colorTextCorrect;
	public Color m_colorTextMiss;
	public Color m_colorTextSuccess;

	public Text m_textScore;
	public Text m_textLevel;
	public Text m_textHit;
	public Text m_textHit2;
	public Text m_textMiss;
	public Text m_textMessage;
	public Text m_textModes;
	public Text m_textModes2;
	public Text m_textTries2;

	public List<int> m_listPrices;

	public List<string> m_listModes;

	public GameObject m_objectBall;
	public GameObject m_objectBallRoot;

	public GameObject m_objectTutorialArrow;

	public GAME_STATE m_eState;
	GameObject m_currentEnemy;

	public List<GameObject> m_listEnemies;

	public List<GameObject> m_listLeftObjects;
	public List<GameObject> m_listRightObjects;

	public GameObject m_objectBottleHolder;

	//public GameObject m_objectTutorialFinger;

	public List<PlatformObject> m_listPlatforms;
	int m_countPlatforms;

	public List<Vector3> m_listPlatformStats;
	public List<GameObject> m_listPlatformPrefabObjects;
	public List<Vector3> m_listPlatformCityStats;
	public List<CityModeLevel> m_listCityStats;

	public List<int> m_listPlatformStatsControlledRandom;
	public List<int> m_listPlatformStatsControlledRandomRelax;

	public GameObject m_objectSplashBackground;

	public TutorialHandAnimator m_scriptTutorialHand;

	public GameObject m_objectCoin;

	public GameObject m_bottleHolder;
	public GameObject m_markerHolder;
	public GameObject m_tableHolder;

	public PhysicsMaterial2D m_bottlePhysicMaterial;

	int m_isWatchVideo;

	Vector3 m_startPosition;
	Vector2 m_positionTouch;

	int m_currentHighscore;
	int m_currentLevel;
	int m_currentScore;
	int m_currentAds;
	int m_currentHits;
	int m_currentMode;
	int m_currentBird;
	int m_currentPurchaseCount;
	int m_currentAttempts;

	float m_previousPosition = 0;

	float m_currentIncentifiedAdsTime;
	float m_currentDirectionTime;

	float m_currentLeftGroupPosition = -999;
	float m_currentRightGroupPosition = -999;

	float m_currentAdsTime;

	bool m_isSuccess = false;

	string m_stringSuffix = " FLIPS";

	bool m_animateIn = true;

	void Awake()
	{
		instance = this;

		Application.targetFrameRate = 60;

		if (!PlayerPrefs.HasKey ("Hit")) {
			PlayerPrefs.SetInt ("Hit", 0);
		}

		if (!PlayerPrefs.HasKey ("IsFreshInstall3")) {
			PlayerPrefs.SetInt ("IsFreshInstall3", 0);
			if (PlayerPrefs.HasKey ("Mode")) {
				PlayerPrefs.SetInt ("Mode", 10);
			}
		}
		if (!PlayerPrefs.HasKey ("Mode")) {
			PlayerPrefs.SetInt ("Mode", 0);
			PlayerPrefs.SetInt ("IsFreshInstall3", 0);
		}


		if (!PlayerPrefs.HasKey ("Removeads")) {
			PlayerPrefs.SetInt ("Removeads", 0);
		}

		if (!PlayerPrefs.HasKey ("currentBird")) {
			PlayerPrefs.SetInt ("currentBird", 0);
		}

		if (!PlayerPrefs.HasKey ("currentPurchaseCount")) {
			PlayerPrefs.SetInt ("currentPurchaseCount", 0);
		}

		if (!PlayerPrefs.HasKey ("attempts")) {
			PlayerPrefs.SetInt ("attempts", 0);
		}

		if (!PlayerPrefs.HasKey ("achievement_onetries")) {
			PlayerPrefs.SetInt ("achievement_onetries", 0);
		}

		for (int x = 0; x < 20; x++) {
			if (x == 0) {
				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
					PlayerPrefs.SetInt ("birdbought" + x, 1);
				}
			} else {
				if (!PlayerPrefs.HasKey ("birdbought" + x)) {
					PlayerPrefs.SetInt ("birdbought" + x, 0);
				}
			}
		}

		for (int x = 0; x < 10; x++) {
			if (x == 0) {
				if (!PlayerPrefs.HasKey ("ballbought" + x)) {
					PlayerPrefs.SetInt ("ballbought" + x, 1);
				}
			} else {
				if (!PlayerPrefs.HasKey ("ballbought" + x)) {
					PlayerPrefs.SetInt ("ballbought" + x, 0);
				}
			}
		}

		PlayerPrefs.Save ();

		ZAudioMgr.Instance.enabled = true;
		ZAdsMgr.Instance.enabled = true;
		ZIAPMgr.Instance.enabled = true;
		ZPlatformCenterMgr.Instance.enabled = true;
		ZNotificationMgr.Instance.enabled = true;
	}

	public GameObject m_objectFloorObject;

	void Start()
	{
		if (!PlayerPrefs.HasKey ("Level")) {
			PlayerPrefs.SetInt ("Level", 1);
		} 
		if (!PlayerPrefs.HasKey ("LevelQuick")) {
			PlayerPrefs.SetInt ("LevelQuick", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelPupu")) {
			PlayerPrefs.SetInt ("LevelPupu", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelEndless")) {
			PlayerPrefs.SetInt ("LevelEndless", 0);
		}
		if (!PlayerPrefs.HasKey ("LevelRelax")) {
			PlayerPrefs.SetInt ("LevelRelax", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelCurve")) {
			PlayerPrefs.SetInt ("LevelCurve", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelImpossible")) {
			PlayerPrefs.SetInt ("LevelImpossible", 0);
		}
		if (!PlayerPrefs.HasKey ("LevelSpace")) {
			PlayerPrefs.SetInt ("LevelSpace", 1);
		} 
		if (!PlayerPrefs.HasKey ("LevelCap")) {
			PlayerPrefs.SetInt ("LevelCap", 0);
		}
		if (!PlayerPrefs.HasKey ("LevelRange")) {
			PlayerPrefs.SetInt ("LevelRange", 0);                                                                                                                    
		}
		if (!PlayerPrefs.HasKey ("LevelCapLevel")) {
			PlayerPrefs.SetInt ("LevelCapLevel", 1);
		} 
		if(!PlayerPrefs.HasKey ("LevelCapLevel")) {
			PlayerPrefs.SetInt ("LevelCapLevel", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelCapLevel")) {
			PlayerPrefs.SetInt ("LevelTower", 0);
		}
		if (!PlayerPrefs.HasKey ("LevelCapLevel")) {
			PlayerPrefs.SetInt ("LevelChallenge", 0);
		}

		// Added By Josh
		if (!PlayerPrefs.HasKey ("LevelCity")) 
		{
			PlayerPrefs.SetInt ("LevelCity", 1);
		}
		if (!PlayerPrefs.HasKey ("LevelCapLevel")) 
		{
			PlayerPrefs.SetInt ("LevelMarker", 0);
		}
		if (!PlayerPrefs.HasKey ("LevelCapLevel")) 
		{
			PlayerPrefs.SetInt ("LevelDrone", 0);
		}
		if (!PlayerPrefs.HasKey ("LevelCapLevel")) 
		{
			PlayerPrefs.SetInt ("LevelTable", 0);
		}
			
		if (PlayerPrefs.GetInt ("Removeads") <= 0) {
			ZAdsMgr.Instance.ShowBanner (BANNER_TYPE.BOTTOM_BANNER);
		}



		for (int x = 0; x < GameScene.instance.m_listModes.Count; x++) {
			if (!PlayerPrefs.HasKey ("Attempts" + x)) {
				PlayerPrefs.SetInt ("Attempts" + x, 0);
			}
		}

//		PlayerPrefs.SetInt ("LevelCity", 1);
		ZPlatformCenterMgr.Instance.Login ();

		//ZObjectMgr.Instance.AddNewObject (m_prefabEnemy.name, 20, "");
		//ZObjectMgr.Instance.AddNewObject (m_prefabBall.name, 20, "");
		foreach (GameObject objSpawn in m_listPlatformPrefabObjects) 
		{
			ZObjectMgr.Instance.AddNewObject (objSpawn.name, 2, "");
		}

//		foreach (GameObject citySpawn in )

		m_currentPurchaseCount = PlayerPrefs.GetInt ("currentPurchaseCount");
		m_currentHits = PlayerPrefs.GetInt ("Hit");
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_currentBird = PlayerPrefs.GetInt ("currentBird");

		//Color splashColor = m_objectSplash.GetComponent<SpriteRenderer> ().color;
		//m_objectSplash.GetComponent<SpriteRenderer> ().color = new Color (splashColor.r, splashColor.g, splashColor.b, 1);
		//LeanTween.alpha (m_objectSplash, 0, 0.2f);

		//m_objectTulong.SetActive (false);
		//if (!PlayerPrefs.HasKey ("IsFreshInstall") && PlayerPrefs.HasKey ("Mode")) 
		//{
		//	PlayerPrefs.SetInt ("IsFreshInstall", 0);
		//	m_currentMode = 10;
		//} 
		//else 
		//{
		m_currentMode = PlayerPrefs.GetInt ("Mode");
		//}
		m_currentAttempts = PlayerPrefs.GetInt ("Attempts");
		//MTCharacter.instance.m_currentMode = m_currentMode;
		//m_currentLevel = 12;

		m_currentIncentifiedAdsTime = 60;
		m_currentDirectionTime = 0.1f;

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard[m_currentMode].name + " MODE";
		m_textModes2.text = ZGameMgr.instance.m_listLeaderboard[m_currentMode].name + " MODE";

		m_currentCoinLevel = 0;

		m_objectWatchVideo.SetActive (false);


		if (m_currentMode == 9) 
		{
			m_currentChallengeModeTries = 0;
			//PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, 1);
			//PlayerPrefs.Save ();
			m_currentLevel = 1;

			m_textTries2.text = "TRIES 0/" + m_currentTotalChallengeModeTries;

		}


		LoadData ();
		SetupLevel ();
		m_currentAds = 2;
		m_isWatchVideo = 0;

		ZAdsMgr.Instance.RequestInterstitial ();
	//	ZAdsMgr.Instance.RequestInterstitial (ADS_TYPE.CROSS_PROMOTION);



		m_currentAdsTime = 35;

		m_eState = GAME_STATE.START;
	}

	Vector3 currentPosition;
	Vector3 previousPosition;
	Vector3 previousMousePosition;
	int m_currentSpawnCount;

	Vector2 m_initialPosition;
	Vector2 m_finalPosition;

	bool mouseDown = false;

	int testIdx = 0;
	void Update()
	{
		if (ZGameMgr.instance.isPause)
			return;

		if (Input.GetKeyDown (KeyCode.H)) 
		{
			testIdx += 1;
			ChangeColor (testIdx);
			//AddCoins (1000);
		}

//		if (ZAdsMgr.Instance.IsCrossPromotionAvailable()) 
//		{
//			ZAdsMgr.Instance.ShowCrossPromotion ();
//		}
		
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (m_objectShop.activeSelf) {
				m_objectShop.SetActive (false);
				ZAudioMgr.Instance.PlaySFX (m_audioButton);
			}
			else
				Application.Quit ();
		}

		if (m_eState == GAME_STATE.START)
			return;

		m_currentAdsTime -= Time.deltaTime;

		if (Input.GetKeyDown (KeyCode.J)) 
		{
			ParallaxBg.instance.UpdateBackground (m_objectBottle.transform.position.x);
		}

		if (Application.isEditor) {
			if (Input.GetKeyDown (KeyCode.N)) {
				Score (1);
				Die ();
			}
			if (Input.GetKeyDown (KeyCode.M)) {
				Score (1);
				Score (1);
				Score (1);
				Score (1);
				Score (1);
				Score (1);
				Score (1);
				Score (1);
				Score (1);
				Score (1);
				Die ();
			}
		}

		if (WaterBottle.Instance.isFlipped == 0) 
		{
			//Debug.Log ("Update Flipped");
			if (!mouseDown && Input.GetMouseButtonDown (0)) 
			{
				mouseDown = true;
				m_initialPosition = Input.mousePosition;
				//Debug.Log ("Update MouseDown");
				/*if (m_animateIn) {
					foreach (GameObject btn in m_listButtons) {
						btn.gameObject.SetActive (false);
					}

					foreach (GameObject btn in m_listLeftObjects) {
						//btn.gameObject.SetActive (false);
						//btn.transform.localPosition -= new Vector3(30, 0, 0);
						LeanTween.cancel (btn);
						LeanTween.moveLocal (btn, new Vector3 (m_currentLeftGroupPosition - 450, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
					}

					foreach (GameObject btn in m_listRightObjects) {
						//btn.gameObject.SetActive (false);
						//btn.transform.localPosition -= new Vector3(30, 0, 0);
						LeanTween.cancel (btn);
						LeanTween.moveLocal (btn, new Vector3 (m_currentRightGroupPosition + 450, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
					}

					m_objectSplashBackground.SetActive (false);

					m_objectWatchVideo.SetActive (false);
					m_animateIn = false;

					m_objectTutorial.SetActive (false);



					m_objectTopBar.SetActive (false);
					//m_objectArrowMock.SetActive (true);
					m_objectEarned.SetActive (false);
					m_objectNoInternet.SetActive (false);
					m_objectTutorial.GetComponent<Text> ().text = "";
				}*/
				m_objectTutorial.SetActive (false);
				m_objectEarned.SetActive (false);
				m_objectNoInternet.SetActive (false);
				m_objectTutorialArrow.SetActive (false);
				//m_objectWatchVideo.SetActive (false);
				m_objectTextTutorial.SetActive (false);
				//m_objectTutorialTitle.SetActive (false);
				if (m_currentMode == 1 || m_currentMode == 2 || m_currentMode == 5 || m_currentMode == 6) {
					//m_textScore.text = "" + m_currentScore;
					//m_textLevel.text = "Score";
				}
				//m_currentScore = 0;

			}
			else if (mouseDown && Input.GetMouseButtonUp (0)) 
			{
				mouseDown = false;
				m_finalPosition = Input.mousePosition;


				//if (Input.GetMouseButtonUp (0)) 
				//{
				//currentPosition = Input.mousePosition;

				//previousPosition = m_objectBallRoot.transform.position;//Camera.main.ScreenToWorldPoint (previousPosition);
				//previousPosition = Camera.main.ScreenToWorldPoint (previousMousePosition);
				//currentPosition = Camera.main.ScreenToWorldPoint (currentPosition);

				float angle = Mathf.Atan2 (m_initialPosition.y - m_finalPosition.y, m_initialPosition.x - m_finalPosition.x);
				Vector3 direction = Quaternion.AngleAxis (angle * 180 / Mathf.PI, Vector3.forward) * Vector3.left * 0.5f;	

				//Vector2 newDirection = direction * 4500f;
				Vector2 newDirection = direction * 40000f;

				//if (Vector3.Distance (previousMousePosition, Input.mousePosition) > 20f) {	//newDirection = Vector3.zero;

				//m_objectBall.GetComponent<Rigidbody2D> ().isKinematic = false;
				//m_objectBall.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (newDirection.x * 0.2f, 2000f));//new Vector2(0, 2000f));
				//m_objectBall.GetComponent<Rigidbody2D> ().AddTorque (Random.Range (-550f, 550));
				//LeanTween.scale (m_objectBall, new Vector3 (0.4f, 0.4f, 0.4f), 1f);//.setEase(LeanTweenType.easeInCubic);

				//ZAudioMgr.Instance.PlaySFX (m_audioThrow);

				//m_eState = GAME_STATE.SHOOT;
				//}
				//m_eState = GAME_STATE.IDLE;
				//}


				Vector2 convertedInitialPosition = Camera.main.ScreenToWorldPoint (m_initialPosition);
				Vector2 convertedFinalPosition = Camera.main.ScreenToWorldPoint (m_finalPosition);
				if (Vector2.Distance (convertedInitialPosition, convertedFinalPosition) > 2) 
				{
					m_objectBottle.GetComponent<WaterBottle> ().Flip ();
					if (m_currentMode == 11) 
					{
						m_objectBottle.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (newDirection.x * 0.2f, Vector2.Distance (convertedInitialPosition, convertedFinalPosition) * 700f));
					} 
					else 
					{
						m_objectBottle.GetComponent<Rigidbody2D> ().AddForce (new Vector2 (newDirection.x * 0.2f, Vector2.Distance (convertedInitialPosition, convertedFinalPosition) * 400f));
					}

					if (m_currentMode == 13) 
					{
						float torqueValue = LoadAdditionalTorque (m_finalPosition);
						m_objectBottle.GetComponent<Rigidbody2D> ().AddTorque (2000.0f * torqueValue);
					}
					else
					{
						m_objectBottle.GetComponent<Rigidbody2D> ().AddTorque (1300f);//Vector2.Distance (convertedInitialPosition, convertedFinalPosition) * 140f);
					}
					//Debug.Log ("FLip : " + Vector2.Distance (convertedInitialPosition, convertedFinalPosition));
					ZAudioMgr.Instance.PlaySFX (m_audioThrow);
				}



				//LeanTween.delayedCall (5f, SetupLevel);
			}
		}


		m_currentIncentifiedAdsTime -= Time.deltaTime;

	}

	// + EDIT JOSH 03132017
	// Used by the table mode to identify which side of the screen was used to flipped the object
	public float LoadAdditionalTorque(Vector2 p_touchPos)
	{
		float torqueValue = 0;
		int objCenter = Screen.width / 2;

		if (objCenter > p_touchPos.x) 
		{
			torqueValue = remapValue (p_touchPos.x, 0, objCenter, 1.0f, 0.6f);
			torqueValue *= -1;
		}
		else if (objCenter < p_touchPos.x) 
		{
			torqueValue = remapValue (p_touchPos.x, objCenter, Screen.width, 0.6f, 1.0f);
		}
		return torqueValue;
	}
	private float remapValue(float p_value, float p_from1, float p_to1, float p_from2, float p_to2)
	{
		return (p_value - p_from1) / (p_to1 - p_from1) * (p_to2 - p_from2) + p_from2;
	}

	void LoadData()
	{

	}

	public void Hit(Vector3 position)
	{
		//if (m_eState == GAME_STATE.RESULTS)
		//	return;

		m_objectHitParticle.GetComponent<ParticleSystem> ().Clear ();
		m_objectHitParticle.transform.position = position;
		m_objectHitParticle.SetActive (true);
		m_objectHitParticle.GetComponent<ParticleSystem> ().Play ();

		ZAudioMgr.Instance.PlaySFX (m_audioCatch);
		m_currentSpawnCount--;
		//m_eState = GAME_STATE.IDLE;

		//m_currentHits++;
	}

	public void HitHeadshot(Vector3 position)
	{
	m_objectParticlePerfect.GetComponent<ParticleSystem> ().Clear ();
	m_objectParticlePerfect.transform.position = position;
	m_objectParticlePerfect.SetActive (true);
	m_objectParticlePerfect.GetComponent<ParticleSystem> ().Play ();

		m_objectHeadshot.SetActive (true);
		LeanTween.scale (m_objectHeadshot, new Vector3(1.2f, 1.2f, 1.2f), 0.4f).setEase(LeanTweenType.easeInOutCubic).setLoopCount(2).setLoopPingPong().setOnComplete(HitHeadshotRoutine);
		//LeanTween.scale (m_textScore.gameObject, new Vector3(1.2f, 1.2f, 1.2f), 0.4f).setEase(LeanTweenType.easeInOutCubic).setLoopCount(2).setLoopPingPong().setOnComplete(HitHeadshotRoutine);
		//Score ();

		ZAudioMgr.Instance.PlaySFX (m_audioHeadshot);
		m_currentSpawnCount--;
		//Debug.LogError ("Headshot");
	}

	void HitHeadshotRoutine()
	{
		m_objectHeadshot.SetActive (false);
	}

	int rewardedSource = 0;
	public void Die()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;
		//StopCoroutine ("whileTest");
		m_textScore.color = m_colorTextMiss;
		m_objectMiss.SetActive (true);
		m_objectWatchVideo.SetActive (false);

		StopCoroutine ("whileTest");
		ZAnalytics.Instance.SendLevelFail ("mode" + m_currentMode, m_currentLevel, m_currentScore);

		int chatvalue = Random.Range (0, 9);
		switch (chatvalue) {
		case 0 : 
			m_textMiss.text = "Miss!";
			break;
		case 1 : 
			m_textMiss.text = "Woops!";
			break;
		case 2 : 
			m_textMiss.text = "Oh no!";
			break;
		case 3 : 
			m_textMiss.text = "No!";
			break;
		case 4 : 
			m_textMiss.text = "Argh!";
			break;
		case 5 : 
			m_textMiss.text = "Nooo!";
			break;
		case 6 : 
			m_textMiss.text = "Ow!";
			break;
		default : 
			m_textMiss.text = "Miss!";
			break;
		}
			

		m_currentCoinLevel = 0;

		LeanTween.cancel (m_textMiss.gameObject);
		m_textMiss.transform.localScale = new Vector3 (0.5f, 0.5f, 1f);
		LeanTween.scale (m_textMiss.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);

		//MTCharacter.instance.Die ();
		ZCameraMgr.instance.DoShake ();

		ZAudioMgr.Instance.PlaySFX (m_audioDie);
		//m_textScore.text = "";
		//Debug.LogError ("Miss");

	/*
		if (Advertisement.IsReady ("rewardedVideo") &&
			m_currentScore < m_currentLevel/2 && 
			m_currentIncentifiedAdsTime <= 0 && 
			m_currentLevel > 50 && false) 
		{
			rewardedSource = 0;
			ShowRewardedAdPopup ("One more chance?", "Watch an ad to continue!");
			m_currentIncentifiedAdsTime = 180;
		} else if (Advertisement.IsReady ("rewardedVideo") && m_currentIncentifiedAdsTime <= 0 && m_currentLevel > 3) {
			//ShowAdPopup ("Make Support the Dev!","Click yes to watch an ad!");
			//m_currentIncentifiedAdsTime = 180;

			LeanTween.delayedCall (2f, SetupLevel);
			rewardedSource = 1;
			//ShowRewardedAdPopup ("Get 100 Eagles!", "Click yes to watch an ad!");
			//m_objectWatchVideo.SetActive(true);
			m_isWatchVideo = 2;
			m_currentIncentifiedAdsTime = 180;
		}else {
			LeanTween.delayedCall (2f, SetupLevel);

			m_currentAds++;
			if (m_currentLevel > 4 && 
				m_currentAds > 4 && 
				ZAdsMgr.Instance.removeAds <= 0 ) 
			{
				if (Advertisement.IsReady("video")) 
				{
					Advertisement.Show ("video");
					m_currentAds = 0;
					//m_objectTulong.SetActive (true);
				}
			}
		}*/

		/*m_currentAds++;
		if (m_currentLevel > 5 && 
			m_currentAds > 10 && 
			ZAdsMgr.Instance.removeAds <= 0 ) 
		{
			if (Advertisement.IsReady("video")) 
			{
				Advertisement.Show ("video");
				m_currentAds = 0;
			}
		}*/

		/*int randomMiss = Random.Range (0, 100);
		if( randomMiss > 80 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss);
		else if(  randomMiss > 70 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss1);
		else if(  randomMiss > 60 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss2);
		else if(  randomMiss > 50 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss3);
		else if(  randomMiss > 30 )
			ZAudioMgr.Instance.PlaySFX (m_audioMiss4);
		else
			ZAudioMgr.Instance.PlaySFX (m_audioMiss5);*/
	
		m_eState = GAME_STATE.RESULTS;

		//m_currentEnemy.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
		/*foreach (GameObject obj in m_listBirds) {
			obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
		}*/

		/*foreach (GameObject obj2 in m_listEnemies) {
			if (obj2 && m_objectBall && obj2.transform.position.x > m_objectBall.transform.position.x) {
				//m_currentEnemy.GetComponent<MTEnemy> ().RunFast ();
				//m_currentEnemy.GetComponent<Rigidbody2D> ().velocity = new Vector2 (10f, 0);
				//float enemyScale = m_currentEnemy.GetComponent<MTEnemy> ().m_currentScale;
				//m_currentEnemy.transform.localScale = new Vector3 (-enemyScale, enemyScale, enemyScale);
				if (obj2.GetComponent<MTEnemy> ().m_currentState != MTENEMY_STATES.DIE)
					obj2.GetComponent<MTEnemy> ().Reset (-1, 5f, 0);
				//m_currentEnemy.GetComponent<MTEnemy> ().MoveRight ();
			} else if (obj2 && m_objectBall) {
				//m_currentEnemy.GetComponent<MTEnemy> ().MoveLeft ();
				if (obj2.GetComponent<MTEnemy> ().m_currentState != MTENEMY_STATES.DIE)
					obj2.GetComponent<MTEnemy> ().Reset (1, 5f, 0);
				//m_currentEnemy.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
				//float enemyScale = m_currentEnemy.GetComponent<MTEnemy> ().m_currentScale;
				//m_currentEnemy.transform.localScale = new Vector3 (enemyScale, enemyScale, enemyScale);
			}
		}
		m_listEnemies.Clear ();*/
	
		PlayerPrefs.SetInt ("Hit", m_currentHits);
	m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

		//if (m_currentMode == 0 ){
			if (m_currentScore > PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName)) {
				PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, m_currentScore);
				PlayerPrefs.Save ();

				m_currentLevel = m_currentScore;
			}
		//}

		

		ZAnalytics.Instance.SendEarnCoinsEvent ("mode" + m_currentMode, m_currentLevel - m_currentScore);
		//ZAnalytics.Instance.SendScore (m_currentScore);

		/*if (m_currentHits >= GetCurrentShopPrice ()) {
			ZRewardsMgr.instance.ShowShopButton ();
		}*/
		/*if (Advertisement.IsReady ("rewardedVideo") &&
		    m_currentScore < m_currentLevel / 2 &&
		    m_currentIncentifiedAdsTime <= 0 && 
			m_currentMode == 0 && 
			m_currentLevel > 5 ) {
			LeanTween.delayedCall (1f, ShowContinueRoutine);
		} /*else if (Advertisement.IsReady ("rewardedVideo") &&
			m_currentScore > m_currentLevel / 2 &&
			m_currentIncentifiedAdsTime <= 0 && 
			m_currentMode == 1 && 
			m_currentLevel > 5 ) {
			LeanTween.delayedCall (1f, ShowContinueRoutine);
		}*/
		/*else */
		if (m_currentMode == 9) {
			m_currentChallengeModeTries++;
			m_textTries2.text = "TRIES " + m_currentChallengeModeTries + "/" + m_currentTotalChallengeModeTries;
			
		}
		if (m_currentMode == 9 && m_currentChallengeModeTries >= m_currentTotalChallengeModeTries) {
			ResetChallengeMode ();
			m_textMiss.text = "Finish!";
		} else if (m_currentLevel > 3 && m_currentAds > 7 && m_currentAdsTime <= 0 && ZAdsMgr.Instance.IsInterstitialAvailable()) {
			m_currentAds = 0;
			ZAdsMgr.Instance.ShowInsterstitial ();
			LeanTween.delayedCall (1.75f, SetupLevel);
			m_currentAdsTime = 60;
		}
		else if (m_currentHits >= GetCurrentShopPrice () && 
			m_currentPurchaseCount < m_listShopBirds.Count-1 && 
			ZRewardsMgr.instance.WillShowShopButton()) {
			ZRewardsMgr.instance.ShowShopButton ();
			
			LeanTween.delayedCall (1f, ShowShopAvailable);
		}else if (m_currentLevel > 2 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			ZRewardsMgr.instance.ShowRewardSuccess ();
			LeanTween.delayedCall (2f, SetupLevel);
		}
		else if (m_currentLevel > 2 && ZRewardsMgr.instance.WillShowReward ()) {
			LeanTween.delayedCall (1f, ShowReward);
		} else {
			LeanTween.delayedCall (1f, SetupLevel);
		}
		
		m_currentAds++;

		Debug.Log ("Mode : " + m_currentMode);
		#if UNITY_ANDROID
		//if (m_currentMode == 1) {
	ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
		//}
		#else
		//if (m_currentMode == 1) {
	//Debug.Log("LB Name : " + ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName + " IOS : " + ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
	ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
		//}
		#endif // UNITY_ANDROID

		m_currentAttempts++;
		PlayerPrefs.SetInt ("attempts", m_currentAttempts);

		/*m_currentAds++;
		if (m_currentLevel > 4 && m_currentAds > 5) 
		{
			if (Advertisement.IsReady("video")) 
			{
				Advertisement.Show ("video");
				m_currentAds = 0;
				m_objectTulong.SetActive (true);
			}

			m_currentAds = 0;
		}*/

		
		// Achievment perseverance
		if (m_currentAttempts >= 100) {
			ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQEQ");
		}

		if (m_currentMode == 0 || m_currentMode == 3 || m_currentMode == 4 || m_currentMode == 7 || m_currentMode == 10) 
		{
			int currentAttempts = PlayerPrefs.GetInt ("Attempts" + m_currentMode);
			PlayerPrefs.SetInt ("Attempts" + m_currentMode, currentAttempts + 1);
			m_textTries2.color = m_colorTextCorrect;
			if( m_currentAttempts == 0 )
				m_textTries2.text = "";
			else
				m_textTries2.text = "TRIES " + (currentAttempts + 1);
		} 
		else if (m_currentMode == 9) 
		{
			
		}else {
			m_textTries2.text = "";
		}


		if (m_currentMode == 12) {
			DroneController.instance.Die ();
		}
		
	}

	void ShowContinueRoutine(){
		ShowRewardedAdPopup ("Continue?", "Watch an ad to continue!");
		m_currentIncentifiedAdsTime = 120;
	}

	void ShowShopAvailable(){
		m_objectReadyUnlock.SetActive (true);
		LeanTween.scale (m_objectReadyUnlock, m_objectReadyUnlock.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
		LeanTween.delayedCall (1.505f, SetupLevel);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ShowReward(){
		LeanTween.delayedCall (2f, SetupLevel);
		ZRewardsMgr.instance.ShowReward ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void DieForce()
	{
	}

	public int GetMode()
	{
		return m_currentMode;
	}

	public GameObject m_objectCamera;

	public void MoveCamera(Transform bottlePosition)
	{
		LeanTween.cancel (m_objectCamera);
		if (m_currentMode == 0 || m_currentMode == 4 || m_currentMode == 7) 
		{
			if (isChangeModeFromButton) {
				if (GetCameraLevel () == 0) {
					m_objectCamera.transform.position = new Vector3 (bottlePosition.position.x + 2, m_objectCamera.transform.position.y, m_objectCamera.transform.position.z);
				} else if (GetCameraLevel () == 1) {
					m_objectCamera.transform.position = new Vector3 (bottlePosition.position.x + 4, m_objectCamera.transform.position.y, m_objectCamera.transform.position.z);
				} else if (GetCameraLevel () == 2) {
					m_objectCamera.transform.position = new Vector3 (bottlePosition.position.x + 6, m_objectCamera.transform.position.y, m_objectCamera.transform.position.z);
				} else if (GetCameraLevel () == 3) {
					m_objectCamera.transform.position = new Vector3 (bottlePosition.position.x + 8, m_objectCamera.transform.position.y, m_objectCamera.transform.position.z);
				} else if (GetCameraLevel () == 4) {
					m_objectCamera.transform.position = new Vector3 (bottlePosition.position.x + 12, m_objectCamera.transform.position.y, m_objectCamera.transform.position.z);
				} else {
					m_objectCamera.transform.position = new Vector3 (bottlePosition.position.x + 6, m_objectCamera.transform.position.y, m_objectCamera.transform.position.z);
				}
			} else {
				if (GetCameraLevel () == 0) {
					LeanTween.moveX (m_objectCamera, bottlePosition.position.x + 2, 0.6f);
				} else if (GetCameraLevel () == 1) {
					LeanTween.moveX (m_objectCamera, bottlePosition.position.x + 4, 0.6f);
				} else if (GetCameraLevel () == 2) {
					LeanTween.moveX (m_objectCamera, bottlePosition.position.x + 6, 0.6f);
				} else if (GetCameraLevel () == 3) {
					LeanTween.moveX (m_objectCamera, bottlePosition.position.x + 8, 0.6f);
				} else if (GetCameraLevel () == 4) {
					LeanTween.moveX (m_objectCamera, bottlePosition.position.x + 12, 0.6f);
				} else {
					LeanTween.moveX (m_objectCamera, bottlePosition.position.x + 6, 0.6f);
				}
			}
			m_objectFloorObject.transform.position = new Vector3 (bottlePosition.position.x,
				m_objectFloorObject.transform.position.y, m_objectFloorObject.transform.position.z);
		} else if (m_currentMode == 3) {
			if (isChangeModeFromButton)
				m_objectCamera.transform.position = new Vector3 (bottlePosition.position.x + 2, m_objectCamera.transform.position.y, m_objectCamera.transform.position.z);
			else
				LeanTween.moveX (m_objectCamera, bottlePosition.position.x + 2, 0.6f);
		} 
		else if (m_currentMode == 2 || m_currentMode == 9) 
		{
			//LeanTween.moveX (m_objectCamera, bottlePosition.position.x - 5, 0.6f);
			if (isChangeModeFromButton) 
			{
				m_objectCamera.transform.position = new Vector3 (bottlePosition.position.x + 6, m_objectCamera.transform.position.y, m_objectCamera.transform.position.z);
			} 
			else 
			{
				LeanTween.moveX (m_objectCamera, bottlePosition.position.x + 6, 0.6f);
			}
			m_objectFloorObject.transform.position = new Vector3 (bottlePosition.position.x,
																	m_objectFloorObject.transform.position.y, 
																	m_objectFloorObject.transform.position.z);
		} 
		if (m_currentMode == 10) 
		{
			if (isChangeModeFromButton) 
			{	
				m_objectCamera.transform.position = new Vector3 (bottlePosition.position.x + 8, m_objectCamera.transform.position.y, m_objectCamera.transform.position.z);
				m_objectWindManager.GetComponent<WindManager> ().SetPos (m_objectCamera.transform.position);
			} 
			else 
			{
				LeanTween.moveX (m_objectWindManager, bottlePosition.position.x + 8, 0.6f);
				LeanTween.moveX (m_objectCamera, bottlePosition.position.x + 8, 0.6f);
			}
		}
		else if (m_currentMode == 12) 
		{
			if (isChangeModeFromButton) 
			{	
				m_objectCamera.transform.position = new Vector3 (bottlePosition.position.x + 15, m_objectCamera.transform.position.y, m_objectCamera.transform.position.z);			} 

//			else 
//			{
//			//	LeanTween.moveX (m_objectCamera, bottlePosition.position.x + 15, 0.6f);
//			}
//			m_objectFloorObject.transform.position = new Vector3 (bottlePosition.position.x,
//				m_objectFloorObject.transform.position.y, 
//				m_objectFloorObject.transform.position.z);
//			if (isChangeModeFromButton) 
//			{
//				m_objectCamera.transform.position = new Vector3 (m_objectCamera.transform.position.x, bottlePosition.position.y, m_objectCamera.transform.position.z);
//			} 
//			else 
//			{
//				LeanTween.moveY (m_objectCamera, bottlePosition.position.y, 0.6f);
//				//LeanTween.move (m_objectCamera, new Vector2 (bottlePosition.position.x, bottlePosition.position.y + 6), 0.6f);
//			}
//			m_objectFloorObject.transform.position = new Vector3 (m_objectFloorObject.transform.position.x,
//																bottlePosition.position.y, 
//																m_objectFloorObject.transform.position.z);
		}
		else 
		{
		}

	m_objectFloorDieObject.transform.localPosition = new Vector3 (m_objectCamera.transform.localPosition.x,
		m_objectFloorDieObject.transform.localPosition.y,
		m_objectFloorDieObject.transform.localPosition.z);
	}

	void SetUpBallMode()
	{
		m_objectDroneTable.transform.GetChild (0).GetComponent<DroneController> ().ChageDirection ();
		m_objectBottle.transform.position = m_objectDroneTable.GetComponent<PlatformObject> ().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
		ChangeColor (m_currentScore);
	}

	void SetupEiffel()
	{
		m_objectBottle.transform.position = m_objectEiffel.GetComponent<PlatformObject> ().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);

	}

void ResetChallengeModeScore()
{
	m_currentChallengeModeTries = 0;
}

	void ResetChallengeMode()
	{
		LeanTween.delayedCall (1.5f, ResetChallengeModeScore);
		//PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, 1);
		//PlayerPrefs.Save ();
		m_currentLevel = 0;
		LeanTween.delayedCall (2.5f, SetupLevel);
		m_textTries2.color = m_colorTextCorrect;
		m_textScore.color = m_colorTextCorrect;
	//m_textTries2.text = "TRIES " + m_currentChallengeModeTries + "/" + m_currentTotalChallengeModeTries;
	}

	public void Score(int score)
	{
	if (m_currentMode == 0 || m_currentMode == 3 || m_currentMode == 4 || m_currentMode == 7 || m_currentMode == 9 || m_currentMode == 10) {
			m_currentScore += score;
			m_textScore.text = "" + m_currentScore;
			m_currentLevel = m_currentScore;// + Mathf.FloorToInt((m_currentHighscore/2f));

			GameObject currentPlatform = ResetPlatforms ();
			//m_objectBottle.transform.position = currentPlatform.GetComponent<PlatformObject>().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			//ZAudioMgr.Instance.PlaySFX (m_audioButton);
			UpdateCamera ();
			LeanTween.cancel (m_textScore.gameObject);
			LeanTween.scale (m_textScore.gameObject, m_textScore.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase (LeanTweenType.easeInOutCubic);
			//m_currentCoinLevel = 99;
			//if (m_targetPlatform && m_currentCoinLevel == 99) {
			if (PlayerPrefs.GetInt ("Attempts" + m_currentMode) <= 1) {
				m_objectCoin.SetActive (true);
				m_objectCoin.transform.position = m_targetPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position + new Vector3 (Random.Range (-6, -4), 0, 0);
				m_objectCoin.transform.position = new Vector3 (m_objectCoin.transform.position.x, m_objectCoin.transform.position.y, 10.9f);
			} else if (Random.Range (0, 100) > 90) {
				m_objectCoin.SetActive (true);
				m_objectCoin.transform.position = m_targetPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position + new Vector3 (Random.Range (-6, -4), 0, 0);
				m_objectCoin.transform.position = new Vector3 (m_objectCoin.transform.position.x, m_objectCoin.transform.position.y, 10.9f);
			}

			if (m_currentMode == 9) {
				m_currentChallengeModeTries++;
				m_textTries2.text = "TRIES " + m_currentChallengeModeTries + "/" + m_currentTotalChallengeModeTries;
				if (m_currentMode == 9 && m_currentChallengeModeTries >= m_currentTotalChallengeModeTries) {
					//ResetChallengeMode ();
					m_textMiss.text = "Finish!";
				}
			} else if (m_currentMode == 10) 
			{
				ParallaxBg.instance.UpdateBackground (m_objectBottle.transform.position.x);
				ChangeColor (m_currentScore);
			}
	} 	else if (m_currentMode == 1 || m_currentMode == 5 || m_currentMode == 6 || m_currentMode == 8 || m_currentMode == 11 || m_currentMode == 12 || m_currentMode == 13) {
			m_currentScore += score;
			m_textScore.text = "" + m_currentScore;
			m_currentLevel = m_currentScore;// + Mathf.FloorToInt((m_currentHighscore/2f));
			//SpawnEnemyMain ();
			//ZAudioMgr.Instance.PlaySFX (m_audioButton);
			m_eState = GAME_STATE.IDLE;
			//LeanTween.delayedCall (0.01f, ScoreRoutine);

			if (m_currentMode == 8)
				LeanTween.delayedCall (1f, SetupEiffel);

			if (m_currentMode == 12) 
			{
			//	m_objectDroneTable.transform.GetChild (0).GetComponent<BallMode> ().BottleLanded ();
				LeanTween.delayedCall (1f, SetUpBallMode);
			}

		} else if (m_currentMode == 2) {
			m_currentScore += score;
			m_textScore.text = "" + m_currentScore;
			m_currentLevel = m_currentScore;// + Mathf.FloorToInt((m_currentHighscore/2f));
			//SpawnEnemyMain ();
			
			GameObject currentPlatform = SpawnPlatforms ();
			//m_objectBottle.transform.position = currentPlatform.GetComponent<PlatformObject>().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			//ZAudioMgr.Instance.PlaySFX (m_audioButton);
			UpdateCamera ();
			LeanTween.cancel (m_textScore.gameObject);
			LeanTween.scale (m_textScore.gameObject, m_textScore.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase (LeanTweenType.easeInOutCubic);

			//LeanTween.delayedCall (0.01f, ScoreRoutine);

		} 
//		else if (m_currentMode == 12) 
//		{
//			m_currentScore += score;
//			m_textScore.text = "" + m_currentScore;
//			m_currentLevel = m_currentScore;// + Mathf.FloorToInt((m_currentHighscore/2f));
//
//			m_objectBottle.transform.localEulerAngles = Vector3.zero;
//			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
//
//			UpdateCamera ();
//			LeanTween.cancel (m_textScore.gameObject);
//			LeanTween.scale (m_textScore.gameObject, m_textScore.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase (LeanTweenType.easeInOutCubic);
//			m_objectDroneTable.transform.GetChild (0).GetComponent<BallMode> ().BottleLanded ();
//			
//	}
	else 
	{
			if (m_currentScore <= 0)
				return;
		
			m_currentScore -= score;
			if (m_currentScore < 0)
				m_currentScore = 0;
		
			m_textScore.text = "" + m_currentScore;



			if (m_currentScore <= 0) {
				Success ();
			} else if (m_currentMode == 1) {
			m_eState = GAME_STATE.IDLE;
			} else {
				//LeanTween.delayedCall (0.5f, ScoreRoutine);
				//SpawnEnemyMain ();
			m_eState = GAME_STATE.IDLE;
			}
	}

	ZAudioMgr.Instance.PlaySFX (m_audioScore);

		/*if (m_currentMode == 3) {
			m_eState = GAME_STATE.IDLE;
		}*/

		AddCoins (score);

	if (m_currentScore > PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName)) {
		PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName, m_currentScore);
		PlayerPrefs.Save ();
		m_currentLevel = m_currentScore;
			
	}


		//SpawnBall ();
	//m_objectTrumpsHairHead.SetActive (true);
	LeanTween.scale (m_objectTrumpsHairHead, new Vector3 (1, 1, 1), 0.1f);

	m_objectParticlePerfect.GetComponent<ParticleSystem> ().Clear ();
	m_objectParticlePerfect.transform.position = WaterBottle.Instance.gameObject.transform.position;
	m_objectParticlePerfect.SetActive (true);
	m_objectParticlePerfect.GetComponent<ParticleSystem> ().Play ();
		
	if (m_currentMode == 0 || m_currentMode == 3 || m_currentMode == 4 || m_currentMode == 7 || m_currentMode == 10) {
			//int currentAttempts = PlayerPrefs.GetInt ("Attempts" + m_currentMode);
			int currentTries = PlayerPrefs.GetInt ("Attempts" + m_currentMode);
			currentTries++;
			if (currentTries <= 1)
				m_textTries2.text = "MASTER FLIP AT FIRST TRY!";
			else if (currentTries <= 2)
				m_textTries2.text = "EPIC FLIP IN " + currentTries + " TRIES!";
			else if (currentTries <= 3)
				m_textTries2.text = "AMAZING JUST " + currentTries + " TRIES!";
			else if (currentTries <= 4)
				m_textTries2.text = "NO SWEAT IN " + currentTries + " TRIES!";
			else if (currentTries <= 5)
				m_textTries2.text = "DONE IN " + currentTries + " TRIES!";
			else if (currentTries <= 15)
				m_textTries2.text = "NOT BAD JUST " + currentTries + " TRIES!";
			else if (currentTries >= 30)
				m_textTries2.text = "AT LAST IN " + currentTries + " TRIES!";
			else if (currentTries >= 50)
				m_textTries2.text = "GRANDMA FLIP IN " + currentTries + " TRIES!";
			else
				m_textTries2.text = "COOL FLIP IN " + currentTries + " TRIES!";
			m_textTries2.color = new Color (255, 224, 57, 255);
			PlayerPrefs.SetInt ("Attempts" + m_currentMode, 0);

		} else if (m_currentMode == 9) {
		}else {
		m_textTries2.text = "";
	}

	LeanTween.cancel (m_textTries2.gameObject);
	m_textTries2.transform.localScale = new Vector3 (0.8f, 0.8f, 1f);
	LeanTween.scale (m_textTries2.gameObject, m_textTries2.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);


	if (m_currentMode == 1 || m_currentMode == 2 || m_currentMode == 5 || m_currentMode == 6 || m_currentMode == 8 || m_currentMode == 9
		|| m_currentMode == 11 || m_currentMode == 12 || m_currentMode == 13) {
		//m_textScore.text = "" + m_currentScore;
		m_textLevel.text = "Score";
	}

	// Current level achiement onne tries
	if (m_currentAttempts == 1) {
		PlayerPrefs.SetInt ("achievement_onetries", PlayerPrefs.GetInt ("achievement_onetries") + 1);
	} else {
		PlayerPrefs.SetInt ("achievement_onetries", 0);
	}

	if (PlayerPrefs.GetInt ("achievement_onetries") == 5) {
		ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQEA");
	}

	// Current level achievement section
	if (m_currentLevel == 100) {
		ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQAQ");
	} else if (m_currentLevel == 500) {
		ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQAg");
	} else if (m_currentLevel == 1000) {
		ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQAw");
	} else if (m_currentLevel == 2500) {
		ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQBA");
	} else if (m_currentLevel == 5000) {
		ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQBQ");
	}

	if (m_currentMode == 9 && m_currentChallengeModeTries >= m_currentTotalChallengeModeTries) {
			ResetChallengeMode ();
	}
	//m_currentAds++;

	/*
		// TUNING
		if (m_currentLevel > 50) {
			MTCharacter.instance.SetMaxSpeed (2f);
			MTCharacter.instance.SetSpawnRange (-2f, 10f);
		} else if (m_currentLevel > 25) {
			MTCharacter.instance.SetMaxSpeed (1.9f);
			MTCharacter.instance.SetSpawnRange (-2f, 10f);
		} else if (m_currentLevel > 10) {
			MTCharacter.instance.SetMaxSpeed (1.75f);
			MTCharacter.instance.SetSpawnRange (-2f, 9f);
		} else if (m_currentLevel > 5) {
			MTCharacter.instance.SetMaxSpeed (1.6f);
			MTCharacter.instance.SetSpawnRange (-2f, 8f);
		} else {
			MTCharacter.instance.SetMaxSpeed (1.5f);
			MTCharacter.instance.SetSpawnRange (-2f, 7f);
		}*/
	}

	void Success()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;
		//ZAnalytics.Instance.SendScore (m_currentLevel);
		ZAnalytics.Instance.SendEarnCoinsEvent ("mode" + m_currentMode, m_currentLevel);
		ZAnalytics.Instance.SendLevelComplete ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);
		m_textScore.color = m_colorTextSuccess;
		m_textLevel.color = m_colorTextSuccess;
		ZAudioMgr.Instance.PlaySFX (m_audioDie);

		m_objectSuccess.SetActive (true);
	LeanTween.cancel (m_objectSuccess.gameObject);
	LeanTween.scale (m_objectSuccess.gameObject, m_textMiss.gameObject.transform.localScale + new Vector3 (0.2f, 0.2f, 0.2f), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);


		m_objectParticle.SetActive (true);
		StopCoroutine ("whileTest");
		LeanTween.delayedCall (3.5f, SuccessRoutine);
		m_eState = GAME_STATE.RESULTS;

		//float defaultScale = MTCharacter.instance.m_defaultScale;
		//LeanTween.scale (MTCharacter.instance.gameObject, new Vector3(defaultScale+0.4f, defaultScale+0.4f,defaultScale+0.4f ), 0.2f);

		//m_objectNormal.SetActive (false);
		//m_objectWin.SetActive (true);

		//ZObjectMgr.Instance.ResetAll ();

		m_currentAttempts++;
		PlayerPrefs.SetInt ("attempts", m_currentAttempts);

		//LeanTween.moveLocalY (m_objectHitBow, m_objectHitBow.transform.localPosition.y + 2, 0.5f).setLoopCount (6).setLoopPingPong();

		PlayerPrefs.SetInt ("Hit", m_currentHits);
	m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

		ZAudioMgr.Instance.PlaySFX (m_audioSuccess);

		ZAnalytics.Instance.SendCurrentCoins (m_currentHits);
		LeanTween.scale (m_objectTrumpsHairHead, new Vector3 (1, 1, 1), 0.1f);

		foreach (GameObject obj2 in m_listEnemies) {
			if (obj2 && obj2.transform.position.x > m_objectBall.transform.position.x) {
			//m_currentEnemy.GetComponent<MTEnemy> ().RunFast ();
			//m_currentEnemy.GetComponent<Rigidbody2D> ().velocity = new Vector2 (10f, 0);
			//float enemyScale = m_currentEnemy.GetComponent<MTEnemy> ().m_currentScale;
			//m_currentEnemy.transform.localScale = new Vector3 (-enemyScale, enemyScale, enemyScale);
				if (obj2.GetComponent<MTEnemy> ().m_currentState != MTENEMY_STATES.DIE)
					obj2.GetComponent<MTEnemy> ().Reset (-1, 5f, 0);
			//m_currentEnemy.GetComponent<MTEnemy> ().MoveRight ();
			} else if (obj2) {
			//m_currentEnemy.GetComponent<MTEnemy> ().MoveLeft ();
				if (obj2.GetComponent<MTEnemy> ().m_currentState != MTENEMY_STATES.DIE)
					obj2.GetComponent<MTEnemy> ().Reset (1, 5f, 0);
			//m_currentEnemy.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
			//float enemyScale = m_currentEnemy.GetComponent<MTEnemy> ().m_currentScale;
			//m_currentEnemy.transform.localScale = new Vector3 (enemyScale, enemyScale, enemyScale);
			}
		}
		m_listEnemies.Clear ();

		/*if (ZRateUsMgr.Instance.isRateUs <= 0 && m_currentLevel == 5) {
			ZRateUsMgr.Instance.ShowRateUs();
		}
		else if (ZFollowUsMgr.Instance.isFollowUs <= 0 && m_currentLevel == 10 ) {
			ZFollowUsMgr.Instance.ShowFollowUs();
		}
		/*else if (m_currentIncentifiedAdsTime <= 0 && m_currentLevel > 5) {
			ShowAdPopup ("Make Support the Dev!","Click yes to watch an ad!");
			m_currentIncentifiedAdsTime = 180;
		}*/
		/*else if (Advertisement.IsReady ("rewardedVideo") && m_currentIncentifiedAdsTime <= 0 && m_currentLevel > 3) {
			//ShowAdPopup ("Make Support the Dev!","Click yes to watch an ad!");
			//m_currentIncentifiedAdsTime = 180;

			rewardedSource = 1;
			//ShowRewardedAdPopup ("Get 100 Eagles!", "Click yes to watch an ad!");
			m_currentIncentifiedAdsTime = 180;
			//m_objectWatchVideo.SetActive(true);
			m_isWatchVideo = 2;
		}
		else if (ZRateUsMgr.Instance.isRateUs <= 0 && m_currentLevel % 5 == 0) {
			ZRateUsMgr.Instance.ShowRateUs();
		}
		else if (ZFollowUsMgr.Instance.isFollowUs <= 0 && m_currentLevel % 5 == 0) {
			ZFollowUsMgr.Instance.ShowFollowUs();
		}*/

		if (m_currentMode == 1) {
			foreach (GameObject obj in m_listBirds) {
				obj.GetComponent<Rigidbody2D> ().velocity = new Vector2 (-10f, 0);
			}
		}

		if (m_currentMode == 3) {
			m_isSuccess = true;
			//MTCharacter.instance.Run (4);
			//MTCharacter.instance.RunStraight (4);
			//MTCharacter.instance.Success();
		}

		m_currentAds++;

		#if UNITY_ANDROID
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.PostScore ( PlayerPrefs.GetInt ("Level"), ZGameMgr.instance.LEADERBOARD_ANDROID);
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelQuick"), "CgkI85r1lMsYEAIQAg");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelEndless"), "CgkI85r1lMsYEAIQAw");
		}*/
		ZPlatformCenterMgr.Instance.PostScore(PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);

		#else
		/*if (m_currentMode == 0) {
		ZPlatformCenterMgr.Instance.PostScore ( PlayerPrefs.GetInt ("Level"), ZGameMgr.instance.LEADERBOARD_IOS);
		}
		else if (m_currentMode == 1) {
		ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelQuick"), "highscore_leaderboard_quick");
		}
		else if (m_currentMode == 2) {
		ZPlatformCenterMgr.Instance.PostScore (PlayerPrefs.GetInt ("LevelEndless"), "highscore_leaderboard_endless");
		}*/
		ZPlatformCenterMgr.Instance.PostScore(PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard[m_currentMode].saveName), 
												ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardIOS);
		#endif // UNITY_ANDROID

		
	}

	public void ShowAdPopup(string title, string message)
	{	
		if (ZAdsMgr.Instance.removeAds > 0)
			return;
		
		//if( isRateUs > 0 )
		//	return;
		if (!Advertisement.IsReady ("rewardedVideo"))
			return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowAdPopUpClose;

		//isRateUs++;
	}
	private void OnShowAdPopUpClose(MNDialogResult result) {
		if (result == MNDialogResult.YES) {
			Advertisement.Show ("rewardedVideo");
			//m_objectTulong.SetActive (true);
			//m_currentAds = 0;
		} else {
			//m_currentAds = 2;
		}
	}

	public void ShowRewardedVideo()
	{
		ShowOptions options = new ShowOptions();
		options.resultCallback = AdCallbackhanler;
		Advertisement.Show ("rewardedVideo", options);

		m_objectWatchVideo.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void ShowRewardedAdPopup(string title, string message)
	{	
		//if (ZAdsMgr.Instance.removeAds > 0)
		//	return;
		//if( isRateUs > 0 )
		//	return;
		//if (!Advertisement.IsReady ("rewardedVideo"))
		//	return;

		MobileNativeDialog ratePopUp = new MobileNativeDialog(title, message);
		ratePopUp.OnComplete += OnShowRewardedAdPopUpClose;

		//isRateUs++;
	}
	private void OnShowRewardedAdPopUpClose(MNDialogResult result) 
	{
		if (result == MNDialogResult.YES) 
		{
			ShowOptions options = new ShowOptions();
			options.resultCallback = AdCallbackhanler;
			Advertisement.Show ("rewardedVideo", options);
		} else 
		{
			LeanTween.delayedCall (1f, SetupLevel);
		}
	}

	void AdCallbackhanler(ShowResult result)
	{
		switch (result)
		{
		case ShowResult.Finished:
			//if (rewardedSource == 0)
				ContinueLevel ();
			//if (rewardedSource == 1) {
			//	AddCoins (100);
			//	m_objectEarned.SetActive (true);
				//new MobileNativeMessage("You get 100 Eagles", "Thank you for your Support!");
				//m_objectTulong.SetActive (true);
			//}
			//m_currentAds = 0;
			break;
		case ShowResult.Skipped:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		case ShowResult.Failed:
			LeanTween.delayedCall (1f, SetupLevel);
			break;
		}
	}

	void SuccessRoutine()
	{
		//m_objectSuccess.SetActive (false);
		m_textLevel.text = "Level";
		m_textScore.text = "" + m_currentLevel;
		LeanTween.delayedCall (1f, SuccessRoutine2);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void SuccessRoutine2()
	{
		m_textScore.text = "" + (m_currentLevel+1);
		LeanTween.delayedCall (1.5f, SuccessRoutine3);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		if (m_currentLevel > 1 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			ZRewardsMgr.instance.ShowRewardSuccess ();
		}
	}

	void SuccessRoutine3()
	{
		m_objectSuccess.SetActive (false);
		m_currentLevel++;
		ResetScore();
		//PlayerPrefs.SetInt ("Level", m_currentLevel);

		/*if( m_currentMode == 0 )
			PlayerPrefs.SetInt ("Level", m_currentLevel);
		else if( m_currentMode == 1 )
			PlayerPrefs.SetInt ("LevelQuick", m_currentLevel);
		else if( m_currentMode == 2 )
			PlayerPrefs.SetInt ("LevelEndless", m_currentLevel);*/

		PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName, m_currentLevel);

		/*if (m_currentLevel > 2 && ZRewardsMgr.instance.WillShowRewardSuccess ()) {
			//LeanTween.delayedCall (1f, ShowReward);
			ZRewardsMgr.instance.ShowRewardSuccess ();
			LeanTween.delayedCall (1f, SetupLevel);
		} else {*/
			SetupLevel ();
		//}

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void ScoreRoutine()
	{
		//if( m_currentMode != 1)
		//	SpawnEnemy ();
	}

	void SpawnEnemyQuick()
	{
		if (m_eState == GAME_STATE.RESULTS)
			return;

		StartCoroutine("whileTest");
		//SpawnEnemy ();
	}

	public int GetLevel(){
	if (m_currentMode == 2 || m_currentMode == 12) {
			return m_currentScore;
		} else {
			return m_currentLevel;
		}
	}

	IEnumerator whileTest()
	{
		while(true == true)
		{
		/*
			if(m_currentLevel > 50)
				yield return new WaitForSeconds(Random.Range (5f, 6f));
			else if(m_currentLevel > 25)
				yield return new WaitForSeconds(Random.Range (5f, 6.5f));
			else
				yield return new WaitForSeconds(Random.Range (5f, 7f));
				*/

			//SpawnEnemy ();
		//Random.Range (1.4f, 2.5f
			yield return new WaitForSeconds(Random.Range (1.8f, 2.2f));
			Debug.Log ("while test working");
		}

	}

	public void Play()
	{	
		//if( m_currentMode == 0 || m_currentMode == 2)// || m_currentMode == 3 )// || m_currentMode == 3)
		//	SpawnEnemy (0, true);
	//if (m_currentMode == 1)
		//	SpawnEnemy (0, true);
		//	StartCoroutine("whileTest");
	//	SpawnEnemyQuick ();

		if (m_currentMode == 1) {
			//m_currentScore = 0;
			//m_textScore.text = "" + m_currentScore;
			m_currentHighscore = m_currentLevel;
		}



		//m_objectTutorialFinger.SetActive (true)
		//SpawnEnemy ();

		//if (m_objectTutorialFinger.GetComponent<SpriteRenderer> ().color.a == 0) {
		//	LeanTween.alpha (m_objectTutorialFinger, 1, 0.2f);
		//}

		
		//m_objectBall.GetComponent<Rigidbody2D> ().isKinematic = true;
		m_eState = GAME_STATE.IDLE;
		//m_textLevel.text = "";
		m_textScore.color = m_colorTextCorrect;
		//m_objectTulong.SetActive (false);
		//m_textHit.gameObject.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		
		m_objectTopBar.SetActive(false);
		//m_objectArrowMock.SetActive (true);
		m_objectEarned.SetActive (false);
		m_objectNoInternet.SetActive (false);

	//m_objectTutorial.GetComponent<Text> ().text = "HOLD TO AIM\n RELEASE TO DROP";

		if (m_animateIn) {
			foreach (GameObject btn in m_listButtons) {
				btn.gameObject.SetActive (false);
			}

			foreach (GameObject btn in m_listLeftObjects) {
				//btn.gameObject.SetActive (false);
				//btn.transform.localPosition -= new Vector3(30, 0, 0);
				LeanTween.cancel(btn);
				LeanTween.moveLocal (btn, new Vector3(m_currentLeftGroupPosition - 450, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
			}

			foreach (GameObject btn in m_listRightObjects) {
				//btn.gameObject.SetActive (false);
				//btn.transform.localPosition -= new Vector3(30, 0, 0);
				LeanTween.cancel(btn);
				LeanTween.moveLocal (btn, new Vector3(m_currentRightGroupPosition + 450, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
			}

			m_objectSplashBackground.SetActive (false);

			m_objectWatchVideo.SetActive (false);
			m_animateIn = false;
		}


		//SpawnEnemyMain ();
		m_eState = GAME_STATE.IDLE;
		//m_textLevel.text = "";
		m_textScore.color = m_colorTextCorrect;
		//m_objectTulong.SetActive (false);
		//m_textHit.gameObject.SetActive (false);
		//Play ();
		//m_objectArrowMock.SetActive (true);

		//return;
		
		
		

	}

bool isChangeModeFromButton = false;
	public void ChangeMode()
	{
		isChangeModeFromButton = true;
		m_currentMode++;
		if (m_currentMode >= ZGameMgr.instance.m_listLeaderboard.Count) 
		{
			m_currentMode = 0;
		}
				
		//MTCharacter.instance.m_currentMode = m_currentMode;

		RefreshMode ();

		ZAnalytics.Instance.SendModesButton ();

	/*
		if (m_currentMode == 3) {
			MTCharacter.instance.PoopRun ();
		} else {
			MTCharacter.instance.Run (4);
		}*/
		//MTCharacter.instance.Reset ();

		m_textModes.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name + " MODE";
		m_textModes2.text = ZGameMgr.instance.m_listLeaderboard [m_currentMode].name + " MODE";

		

		PlayerPrefs.SetInt ("Mode", m_currentMode);

		ResetModeText ();

		SetupLevel ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		isChangeModeFromButton = false;
	}

	void ResetModeText()
	{
		if (m_currentMode == 0)
			m_textLevel.text = "LEVEL";
	else if (m_currentMode == 1 || m_currentMode == 11 || m_currentMode == 13)
			m_textLevel.text = "Highscore";
		else if (m_currentMode == 2 || m_currentMode == 5 || m_currentMode == 6 || m_currentMode == 8 || m_currentMode == 12) 
		{
			m_textLevel.text = "Highscore";
		} 
		else if (m_currentMode == 9) 
		{
			if (m_currentMode == 9 && m_currentChallengeModeTries == 0) 
			{
					m_textLevel.text = "Highscore";
			} 
			else 
			{
				m_textLevel.text = "Score";
			}
		}
		else
			m_textLevel.text = "LEVEL";
	}

	public void RemoveAds()
	{
		ZIAPMgr.Instance.PurchaseRemoveAds ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendNoAdsButton ();
	}

	public void FollowUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.FOLLOWUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public string ScreenshotName = "archereagle_screenshot.png";
	string m_screenshotText;
	string m_screenshotPath;

	public void ShareScreenshotWithText(string text)
	{
		m_screenshotText = text;
		m_screenshotPath = Application.persistentDataPath + "/" + ScreenshotName;
		Application.CaptureScreenshot(ScreenshotName);

		//LeanTween.delayedCall(1f,ShareScreenshotRoutine);
		StartCoroutine("ScreenshotWriteCheck");
	}

	IEnumerator ScreenshotWriteCheck()
	{
		while (true == true) {
			if (System.IO.File.Exists(m_screenshotPath)) {
				ShareScreenshotRoutine ();
				yield return null;
			} else {
				yield return new WaitForSeconds (0.5f);
			}
		}
	}

	void ShareScreenshotRoutine ()
	{
		#if UNITY_ANDROID
		StartCoroutine(AndroidShare (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG));
		#else
		Share (m_screenshotText, m_screenshotPath, "", ZGameMgr.instance.GAME_HASHTAG);
		#endif 
		StopCoroutine("ScreenshotWriteCheck");

	}

	private IEnumerator AndroidShare(string shareText, string imagePath, string url, string subject = "")
	{
		AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);

		return null;
	}

	public void Share(string shareText, string imagePath, string url, string subject = "")
	{
		#if UNITY_ANDROID
		/*AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
		AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

		intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
		AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
		AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
		intentObject.Call<AndroidJavaObject>("setType", "image/png");

		intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareText);

		AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

		AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, subject);
		currentActivity.Call("startActivity", jChooser);*/
		#elif UNITY_IOS
		CallSocialShareAdvanced(shareText, subject, url, imagePath);
		#else
		Debug.Log("No sharing set up for this platform.");
		#endif
	}

	#if UNITY_IOS
	public struct ConfigStruct
	{
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}

	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}

	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt; 
		conf.url = url;
		conf.image = img;
		conf.subject = subject;

		showSocialSharing(ref conf);
	}
	#endif

	public void SharePhoto()
	{
		//ShareScreenshotWithText ("");
		//ZFollowUsMgr.Instance.ShareScreenshotWithText ();
		#if UNITY_ANDROID
		if (UniAndroidPermission.IsPermitted (AndroidPermission.WRITE_EXTERNAL_STORAGE)) {
			ZFollowUsMgr.Instance.Share ();
			ZAnalytics.Instance.SendShareButton ();
		} else {
			UniAndroidPermission.RequestPremission (AndroidPermission.WRITE_EXTERNAL_STORAGE, () => {
				ZFollowUsMgr.Instance.Share ();
				ZAnalytics.Instance.SendShareButton ();
			}, () => {
			// add not permit action
				ZFollowUsMgr.Instance.Share ();
				ZAnalytics.Instance.SendShareButton ();
			});
		}
		#else
		ZFollowUsMgr.Instance.Share ();
		ZAnalytics.Instance.SendShareButton ();
		#endif
	}

	public void OpenLeaderboard()
	{
		#if UNITY_ANDROID
		/*if (m_currentMode == 0) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.LEADERBOARD_ANDROID);
		}
		else if (m_currentMode == 1) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAg");
		}
		else if (m_currentMode == 2) {
			ZPlatformCenterMgr.Instance.ShowLeaderboardUI("CgkI85r1lMsYEAIQAw");
		}*/
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.m_listLeaderboard[m_currentMode].leaderboardAndroid);
		#else
		ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		#endif // UNITY_ANDROID
	
	}

	public void LikeUs()
	{
		//ZPlatformCenterMgr.Instance.Login ();
		//return;
		//ShareScreenshotWithText ("HELLO");
		//ZPlatformCenterMgr.Instance.PostScore(10);
		//return;

		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.LIKEUS_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public GameObject m_objectShop;

	public void ShowShop()
	{
		//ZGameMgr.instance.ShowScene ("ShopScene");
		m_objectShop.SetActive(true);
		ShopScene.instance.SetupScene();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		
		ZAnalytics.Instance.SendShopButton ();

		m_eState = GAME_STATE.START;
		mouseDown = false;
	}

	public void RateUs()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.RATEUS_URL_ANDROID);
		#else
		MNIOSNative.RedirectToAppStoreRatingPage(ZGameMgr.instance.RATEUS_URL_IOS);
		#endif

		ZAnalytics.Instance.SendRateUsButton ();
	}

	public void BuyCoins()
	{
		ZIAPMgr.Instance.PurchaseCoins ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);


	}

	public void Resume(){
		ZGameMgr.instance.Resume ();
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void AddCoins(int index, bool playSound=false)
	{
		m_currentHits += index;
		PlayerPrefs.SetInt ("Hit", m_currentHits);
	
		m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
		m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;

		if (playSound) {
			ZAudioMgr.Instance.PlaySFX (m_audioCoin);
		}

		/*if (m_currentHits >= GetCurrentShopPrice () && m_eState == GAME_STATE.START && m_currentPurchaseCount < m_listShopBirds.Count) {
			LeanTween.delayedCall (0.2f, AddCoinRoutine);
		}*/
	}

	void AddCoinRoutine()
	{
		ZRewardsMgr.instance.ShowShopButton (true);
		LeanTween.delayedCall (1f, ShowShopAvailable);
	}

	public void RestoreAds()
	{
		//ShareScreenshotWithText ("HELLO");
		//ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		//return;

		ZIAPMgr.Instance.RestorePurchases ();

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendRestoreIAPButton ();
	}

	public void MoreGames()
	{
		#if UNITY_ANDROID
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_ANDROID);
		#else
		Application.OpenURL (ZGameMgr.instance.MOREGAMES_URL_IOS);
		#endif

		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		ZAnalytics.Instance.SendMoreGamesButton ();
	}

	void SpawnViaMode()
	{
		switch(m_currentMode)
		{
		case 0:
			break;
		case 1: 
			break;
		case 2:
			break;
		}

		//m_objectHeadshot.SetActive (false);
	}

	public List<GameObject> m_listBirds;



	public void UseBird(int count)
	{
		if (PlayerPrefs.GetInt ("birdbought" + count) > 0) {
			m_currentBird = count;
			PlayerPrefs.SetInt ("currentBird", m_currentBird);
			m_objectShop.SetActive (false);
			SetupBird ();
			ZAudioMgr.Instance.PlaySFX (m_audioButton);
			ZAnalytics.Instance.SendUseChar (count);
		} else {
			BuyBird (count);
			ZAudioMgr.Instance.PlaySFX (m_audioBuy);
		}

		m_eState = GAME_STATE.IDLE;
	}

	void SetupBird()
	{
		int x = 0;
		foreach (ShopBird objBird in m_listShopBirds) 
		{
			if (x == m_currentBird) 
			{
				objBird.objectImage.SetActive (true);
			} else {
				objBird.objectImage.SetActive (false);
			}
			x++;
		}
	}

	public int GetCurrentShopPrice()
	{
		return m_listPrices [m_currentPurchaseCount];
	}

	public void BuyBird(int count)
	{
		if (m_currentHits >= m_listPrices[m_currentPurchaseCount] && PlayerPrefs.GetInt("birdbought" + count) == 0) {
			m_currentHits -= m_listPrices[m_currentPurchaseCount];
			PlayerPrefs.SetInt ("Hit", m_currentHits);
			PlayerPrefs.SetInt ("birdbought" + count, 1);

			ZAnalytics.Instance.SendSpendShopItemEvent ("birdbought" + count, m_listPrices [m_currentPurchaseCount]);
			ZAnalytics.Instance.SendCharComplete ("char", m_currentPurchaseCount);
			
			m_currentPurchaseCount++;
			PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
			PlayerPrefs.Save ();

			//UseBird(count);
			m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
			m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
			//ZAudioMgr.Instance.PlaySFX (m_audioButton);

			ShopScene.instance.SetupScene (true, true);

			int x = 0;
			int currentBirdCount = 0;
			foreach (ShopBird objBird in m_listShopBirds) {
				if (PlayerPrefs.GetInt ("birdbought" + x) == 1) {
					currentBirdCount++;
				}
				x++;
			}

			if (currentBirdCount >= 15) {
				ZPlatformCenterMgr.Instance.PostUnlockAchievement ("CgkI3tKnh7YQEAIQEg");
			}
		}
	}

	public BoxCollider2D m_bottleCollider;
	public BoxCollider2D m_bottleSideCollider;
	public BoxCollider2D m_bottleEndCollider;

	public void OpenAchievements()
	{
		//UniAndroidPermission.IsPermitted (AndroidPermission.WRITE_EXTERNAL_STORAGE);
		/*UniAndroidPermission.RequestPremission (AndroidPermission.WRITE_EXTERNAL_STORAGE, () => {
		// add permit action
		}, () => {
			// add not permit action
		});*/

		ZPlatformCenterMgr.Instance.OpenAchievements ();
	}

	public void BuyBall(int count)
	{
		if (m_currentHits >= 250  && PlayerPrefs.GetInt("ballbought" + count) == 0) {
			m_currentHits -= 250;
			PlayerPrefs.SetInt ("Hit", m_currentHits);
			PlayerPrefs.SetInt ("ballbought" + count, 1);

			ZAnalytics.Instance.SendSpendShopItemEvent ("ballbought" + count, m_listPrices [m_currentPurchaseCount]);
			ZAnalytics.Instance.SendCharComplete ("ball", m_currentPurchaseCount);

			//m_currentPurchaseCount++;
			//PlayerPrefs.SetInt ("currentPurchaseCount", m_currentPurchaseCount);
			//PlayerPrefs.Save ();

			//UseBird(count);
			m_textHit.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
			m_textHit2.text = m_currentHits + "/" + GetCurrentShopPrice () + m_stringSuffix;
	//ZAudioMgr.Instance.PlaySFX (m_audioButton);randomVerticalPosition

			ShopScene.instance.SetupScene ();
		}

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	void RefreshMode()
	{	//PlayerPrefs.SetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName, m_currentLevel);
		/*if( m_currentMode == 0 )
			m_currentLevel = PlayerPrefs.GetInt ("Level");
			//m_currentLevel = 60;
		else if( m_currentMode == 1 )
			m_currentLevel = PlayerPrefs.GetInt ("LevelQuick");
			//m_currentLevel = 75;
		else if( m_currentMode == 2 )
			m_currentLevel = PlayerPrefs.GetInt ("LevelEndless");
			//m_currentLevel = 75;*/

		if (m_currentMode == 9 ) 
		{
			if (m_currentChallengeModeTries == 0) 
			{
				m_currentLevel = 0;
			}
		} 
		else 
		{
			m_currentLevel = PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
		}

		/*if (m_currentMode == 3) {
			m_objectTutorial.GetComponent<Text> ().text = "SWIPE THE BALL UP";
	} else {SetupLevel(
			m_objectTutorial.GetComponent<Text> ().text = "SWIPE THE BALL UP";
		}*/
		//m_objectTutorial.GetComponent<Text> ().text = "TAP TO PLAY";
		ResetScore ();
	}

	int m_currentTotalChallengeModeTries = 30;
	int m_currentChallengeModeTries = 0;
	
	public void SetupLevel()
	{
		Debug.Log ("CurrentMode: " + m_currentMode);
		/*if( m_isWatchVideo == 2 ){
			m_isWatchVideo = 0;
			m_objectWatchVideo.SetActive (true);
			LeanTween.delayedCall (1, SetupLevel);
	ZAudioMgr.Instance.PlaySFX (m_audioButton);SetupLevel(
			return;
		}*/

		//m_objectBottle.GetComponent<WaterBottle> ().isFlipped = 0;
		
		

		ZObjectMgr.Instance.ResetAll ();
		
		
		//m_objectShop.SetActive(false);

		RefreshMode ();
		ResetModeText ();

		m_textLevel.color = m_colorTextSuccess;
		m_textScore.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		//m_objectArrowMock.SetActive (false);
		m_textHit.gameObject.SetActive (true);
		m_objectReadyUnlock.SetActive (false);
		//Debug.LogError ("Setup Level");
		m_objectHeadshot.SetActive (false);
		m_objectParticle.SetActive (false);
		//m_objectTutorialTitle.SetActive (true);
		//m_objectTarget.SetActive (false);
	//m_objectTutorialFinger.SetActive (false);

		//if (m_objectTutorialFinger.GetComponent<SpriteRenderer> ().color.a == 1) {
		//	LeanTween.alpha (m_objectTutorialFinger, 0, 0.2f);
		//}

		//WindManager.Instance ().gameObject.SetActive (false);
		
		
		//m_objectEarned.SetActive (false);

		//m_objectNormal.SetActive (true);
		//m_objectWin.SetActive (false);

		ZAudioMgr.Instance.PlaySFX (m_audioButton);


		/*if (m_isWatchVideo > 0) {
			//m_objectWatchVideo.SetActive (true);
			m_isWatchVideo = 0;
		} else {
			m_objectWatchVideo.SetActive (false);
		}*/
		//MTCharacter.instance.Reset ();
		m_currentSpawnCount = 0;

		if(m_currentMode == 3 )
			m_isSuccess = false;



		//ShowHomeUI ();
		if (m_currentLeftGroupPosition == -999) {
			m_currentLeftGroupPosition = m_listLeftObjects [0].transform.localPosition.x;
		}
		if (m_currentRightGroupPosition == -999) {
			m_currentRightGroupPosition = m_listRightObjects [0].transform.localPosition.x;
		}

		//LeanTween.scale (m_objectTrumpsHairHead, new Vector3 (1, 1, 1), 0.1f);
		//m_objectTrumpsHairBird.transform.localScale = Vector3.zero;

		//SpawnBall ();
		ZAnalytics.Instance.SendLevelStart ("mode" + m_currentMode, m_currentLevel, m_currentAttempts);

		//m_objectEndlessReverseTable.SetActive (false);

		if (m_currentMode == 0) {
			GameObject currentPlatform = ResetPlatforms ();
			m_materialBackground.SetActive (true);

			/*foreach (PlatformObject obj in m_listPlatforms) {
			obj.gameObject.SetActive (false);
		}
		m_listPlatforms [m_currentLevel].gameObject.SetActive (true);
		m_listPlatforms [m_currentLevel+1].gameObject.SetActive (true);
		*/
			m_objectBottle.transform.position = currentPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);
			m_objectFreeRangeTable.SetActive (false);
			m_objectMarkerTable.gameObject.SetActive (false);
			m_objectTableMode.SetActive (false);
			m_objectEndlessTable.SetActive (false);
			m_objectEiffel.SetActive (false);
			m_objectBottleHolder.transform.localEulerAngles = Vector3.zero;

			m_bottleCollider.size = new Vector2 (1.77f, 0.61f);
			m_bottleSideCollider.size = new Vector2 (2.41f, 2.56f);
			
			// Restore marker mode
			m_objectBottle.GetComponent<Collider2D> ().sharedMaterial = null;
			m_bottleEndCollider.gameObject.SetActive (true);
		
			if (!m_bottleSideCollider.GetComponent<WaterBottleScoreChecker> ()) 
			{
				m_bottleSideCollider.gameObject.AddComponent<WaterBottleScoreChecker> ();
			}

			m_tableHolder.gameObject.SetActive (false);
			m_markerHolder.gameObject.SetActive (false);
			m_bottleHolder.gameObject.SetActive (true);
			
		} else if (m_currentMode == 1) {
			m_objectEndlessTable.SetActive (true);
			m_objectEndlessReverseTable.SetActive (false);
			m_objectEiffel.SetActive (false);
			m_objectBottle.transform.position = m_objectEndlessTable.GetComponent<PlatformObject> ().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);
			m_currentScore = 0;
			m_objectBottleHolder.transform.localEulerAngles = Vector3.zero;

			m_bottleCollider.size = new Vector2 (1.77f, 0.61f);
			m_bottleSideCollider.size = new Vector2 (2.41f, 2.56f);
		} else if (m_currentMode == 2) {
			m_currentPlatformWidth = 0;
			m_currentPlatformIndex = 0;
			GameObject currentPlatform2 = SpawnPlatforms (true);

			m_objectBottle.transform.position = currentPlatform2.GetComponent<PlatformObject> ().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);
			m_objectEndlessTable.SetActive (false);
			m_objectEiffel.SetActive (false);

			SpawnPlatforms ();
			m_currentScore = 0;
			m_objectBottleHolder.transform.localEulerAngles = Vector3.zero;
			m_bottleCollider.size = new Vector2 (1.77f, 0.61f);
			m_bottleSideCollider.size = new Vector2 (2.41f, 2.56f);
		} else if (m_currentMode == 3) {
			GameObject currentPlatform = ResetPlatforms ();

			m_objectBottle.transform.position = currentPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);
			m_objectEndlessTable.SetActive (false);
			m_objectFreeRangeTable.SetActive (false);
			m_objectEiffel.SetActive (false);
			m_objectBottleHolder.transform.localEulerAngles = Vector3.zero;
			m_bottleCollider.size = new Vector2 (1.77f, 0.61f);
			m_bottleSideCollider.size = new Vector2 (2.41f, 2.56f);
		} else if (m_currentMode == 4) {
			GameObject currentPlatform = ResetPlatforms ();

			m_objectBottle.transform.position = currentPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (7f);
			m_objectBottleHolder.transform.localEulerAngles = Vector3.zero;
			m_objectEndlessTable.SetActive (false);
			m_objectFreeRangeTable.SetActive (false);
			m_objectEiffel.SetActive (false);
			m_bottleCollider.size = new Vector2 (1.77f, 0.61f);
		} else if (m_currentMode == 5) {
			m_objectEndlessTable.SetActive (false);
			m_objectEndlessReverseTable.SetActive (true);
			m_objectFreeRangeTable.SetActive (false);
			m_objectEiffel.SetActive (false);
			m_objectBottle.transform.position = m_objectEndlessTable.GetComponent<PlatformObject> ().spawnArea.transform.position + new Vector3 (0, 0.5f, 0);//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);
			m_currentScore = 0;
			m_objectBottleHolder.transform.localEulerAngles = new Vector3 (0, 0, 180f);
			;
			m_bottleCollider.size = new Vector2 (0.87f, 0.61f);
		} else if (m_currentMode == 6) {
			m_objectEndlessTable.SetActive (false);
			m_objectEndlessReverseTable.SetActive (false);
			m_objectFreeRangeTable.SetActive (true);
			m_objectEiffel.SetActive (false);
			m_objectBottle.transform.position = m_objectEndlessTable.GetComponent<PlatformObject> ().spawnArea.transform.position + new Vector3 (0, 3.1f, 0);//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);
			m_currentScore = 0;
			m_objectBottleHolder.transform.localEulerAngles = new Vector3 (0, 0, 0);
			m_bottleCollider.size = new Vector2 (1.77f, 0.61f);
		} else if (m_currentMode == 7) {
			GameObject currentPlatform = ResetPlatforms ();
			
			m_objectBottle.transform.position = currentPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);
			m_objectFreeRangeTable.SetActive (false);
			m_objectEndlessTable.SetActive (false);
			m_objectEiffel.SetActive (false);
			m_objectBottleHolder.transform.localEulerAngles = new Vector3 (0, 0, 180f);
			m_bottleCollider.size = new Vector2 (0.87f, 0.61f);
		} else if (m_currentMode == 8) {
			m_objectEiffel.SetActive (true);
			m_objectEndlessTable.SetActive (false);
			m_objectEndlessReverseTable.SetActive (false);
			m_objectBottle.transform.position = m_objectEiffel.GetComponent<PlatformObject> ().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);
			m_objectBottle.GetComponent<WaterBottle> ().SetAngularDrag (1.4f);
			m_currentScore = 0;
			m_objectBottleHolder.transform.localEulerAngles = Vector3.zero;
			m_bottleCollider.size = new Vector2 (1.77f, 0.61f);
		} else if (m_currentMode == 9) {
			GameObject currentPlatform4 = ResetPlatforms ();

			m_objectBottle.transform.position = currentPlatform4.GetComponent<PlatformObject> ().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);
			m_objectFreeRangeTable.SetActive (false);
			m_objectEndlessTable.SetActive (false);
			m_objectEiffel.SetActive (false);
			m_objectBottleHolder.transform.localEulerAngles = Vector3.zero;
			m_bottleCollider.size = new Vector2 (1.77f, 0.61f);
			m_bottleSideCollider.size = new Vector2 (2.41f, 2.56f);
			
			m_textTries2.text = "TRIES " + m_currentChallengeModeTries + "/" + m_currentTotalChallengeModeTries;
		} else if (m_currentMode == 10) {
			m_objectCity.SetActive (true);
			m_materialBackground.SetActive (false);
			ChangeColor (m_currentScore);
			//m_objectCamera.GetComponent<Camera> ().backgroundColor = m_listModeColor [m_currentMode].background;
			GameObject currentPlatform = ResetPlatforms ();
			m_objectBottle.transform.position = currentPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position;
			m_objectBottle.transform.eulerAngles = Vector3.zero;
			m_bottleCollider.size = new Vector2 (1.77f, 0.61f);
			m_bottleSideCollider.size = new Vector2 (2.41f, 2.56f);
			m_objectWindManager.gameObject.SetActive (true);

			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);

			ParallaxBg.instance.InitBackground (m_objectBottle.transform.position.x);
		} 
		else if (m_currentMode == 11) 
		{
			m_objectCity.SetActive (false);
			m_objectMarkerTable.SetActive (true);
			m_objectEndlessTable.SetActive (false);
			m_objectFreeRangeTable.SetActive (false);
			m_materialBackground.SetActive (false);
			m_objectCamera.GetComponent<Camera> ().backgroundColor = m_listModeColor [m_currentMode].background;

			m_objectBottle.GetComponent<Collider2D> ().sharedMaterial = m_bottlePhysicMaterial;
			m_objectBottle.transform.position = m_objectMarkerTable.GetComponent<PlatformObject> ().spawnArea.transform.position;
			m_objectBottle.transform.localEulerAngles = Vector3.zero;
			m_directionalLight.GetComponent<Light> ().color = new Color32(234,234,227,255);

			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (4f);
			m_objectBottle.GetComponent<WaterBottle> ().SetAngularDrag (1.4f);
			m_objectWindManager.gameObject.SetActive (false);
			m_objectBottleHolder.transform.localEulerAngles = new Vector3 (0, 0, 180f);

			m_bottleCollider.size = new Vector2 (0.75f, 0.61f);
			m_bottleSideCollider.size = new Vector2 (0.75f, 2.56f);
			m_bottleEndCollider.gameObject.SetActive (false);
			m_currentScore = 0;

			m_tableHolder.gameObject.SetActive (false);
			m_markerHolder.gameObject.SetActive (true);
			m_bottleHolder.gameObject.SetActive (false);

		} else if (m_currentMode == 12) {
			m_currentPlatformIndex = 0;
			m_objectBottle.GetComponent<Collider2D> ().sharedMaterial = null;
			m_objectCamera.GetComponent<Camera> ().backgroundColor = m_listModeColor [m_currentMode].background;
			m_objectBottleHolder.transform.localEulerAngles = new Vector3 (0, 0, 0);
			m_materialBackground.SetActive (false);

			m_objectBottle.transform.localEulerAngles = Vector3.zero;
			m_objectDroneTable.SetActive (true);
			m_objectBottle.transform.position = m_objectDroneTable.GetComponent<PlatformObject> ().spawnArea.transform.position;
			m_objectDroneTable.transform.GetChild (0).GetComponent<DroneController> ().ChageDirection ();
			ChangeColor (m_currentScore);

			m_objectMarkerTable.SetActive (false);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);

			m_bottleCollider.size = new Vector2 (1.77f, 0.61f);
			m_bottleSideCollider.size = new Vector2 (2.41f, 2.56f);
			m_objectBottle.GetComponent<Collider2D> ().sharedMaterial = null;
			m_bottleEndCollider.gameObject.SetActive (true);

			m_tableHolder.gameObject.SetActive (false);
			m_markerHolder.gameObject.SetActive (false);
			m_bottleHolder.gameObject.SetActive (true);
			
			m_currentScore = 0;
		} 
		else if (m_currentMode == 13) 
		{
			m_objectCamera.GetComponent<Camera> ().backgroundColor = m_listModeColor [m_currentMode].background;
			m_materialBackground.SetActive (false);

			m_objectDroneTable.SetActive (false);
			m_objectTableMode.SetActive (true);
			m_objectEndlessReverseTable.SetActive (false);
			m_objectEiffel.SetActive (false);
			m_objectBottle.transform.position = m_objectTableMode.GetComponent<PlatformObject> ().spawnArea.transform.position;//m_listPlatforms [m_currentLevel].spawnArea.transform.position;;//new Vector3 (-4.4f, -4.78f, 0);
			m_objectBottle.transform.localEulerAngles = Vector3.zero;//new Vector3 (0, 0, -6.03f);
			m_objectBottle.GetComponent<WaterBottle> ().Reset ();
			m_objectBottle.GetComponent<WaterBottle> ().SetGravity (8f);

			m_currentScore = 0;
			m_objectBottleHolder.transform.localEulerAngles = Vector3.zero;
			m_directionalLight.GetComponent<Light> ().color = new Color32 (234, 234, 227, 255);

			m_objectBottleHolder.transform.localEulerAngles = new Vector3 (0, 0, 0);
			
			if (m_bottleSideCollider.GetComponent<WaterBottleScoreChecker> ()) 
			{
				Destroy (m_bottleSideCollider.GetComponent<WaterBottleScoreChecker> ());
			}
			
			m_bottleCollider.size = new Vector2 (4.3f, 0.61f);
			m_bottleSideCollider.size = new Vector2 (4.3f, 2.56f);
			m_tableHolder.gameObject.SetActive (true);
			m_markerHolder.gameObject.SetActive (false);
			m_bottleHolder.gameObject.SetActive (false);
		}

		//Debug.Log ("SETUPLEVEL : After Resets");
		

		UpdateCamera ();
		PlayerPrefs.Save ();

	m_objectFloorObject.transform.position = new Vector3 (m_objectBottle.transform.position.x,
	m_objectFloorObject.transform.position.y, m_objectFloorObject.transform.position.z);

		if (m_currentLevel <= 2 && m_currentMode == 0) {
			LeanTween.cancel (m_objectTutorialArrow);
			m_objectTutorialArrow.SetActive (true);
			m_objectTutorial.SetActive (true);
			
			m_objectTutorialArrow.transform.position = m_targetPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position;
			m_objectTutorialArrow.GetComponent<ZJumper> ().Move ();
			m_scriptTutorialHand.gameObject.SetActive (true);
			m_scriptTutorialHand.AnimateUpperRight (m_objectBottle.transform.position + new Vector3 (0, 2, -15));
			if (m_currentLevel == 1) {
				m_objectTextTutorial.SetActive (true);
			} else {
				m_objectTextTutorial.SetActive (false);
			}
		} else {
			m_objectTutorialArrow.SetActive (false);
			m_scriptTutorialHand.gameObject.SetActive (false);
			m_objectTextTutorial.SetActive (false);
		}

		m_materialBackground.GetComponent<Renderer>().material.SetColor("_Color", m_listModeColor [m_currentMode].background);
		m_imageTopBar.color = m_listModeColor [m_currentMode].topbar;
		m_imageBackground.color = m_listModeColor [m_currentMode].background;
		m_materialGround.GetComponent<Renderer>().material.SetColor("_Color", m_listModeColor [m_currentMode].ground);
		//m_materialGround2.GetComponent<Renderer>().material.SetColor("_Color", m_listModeColor [m_currentMode].ground);

	if (m_currentMode == 0 || m_currentMode == 3 || m_currentMode == 4 || m_currentMode == 7 || m_currentMode == 10) {
			int currentAttempts = PlayerPrefs.GetInt ("Attempts" + m_currentMode);
			//PlayerPrefs.SetInt ("Attempts" + m_currentMode, currentAttempts + 1);
			if (currentAttempts == 0)
				m_textTries2.text = "";
			else
				m_textTries2.text = "TRIES " + (currentAttempts);
		} else if (m_currentMode == 9) {
		}else {
			m_textTries2.text = "";
		}

	//Debug.Log ("SETUPLEVEL : After Resets 2");

		SetupBird ();

		m_eState = GAME_STATE.IDLE;
	}

	int m_currentCoinLevel = 0;

	public GameObject m_objectEndlessTable;
	public GameObject m_objectEndlessReverseTable;
	public GameObject m_objectFreeRangeTable;
	public GameObject m_objectEiffel;
	public GameObject m_objectMarkerTable;
	public GameObject m_objectDroneTable;
	public GameObject m_objectTableMode;
	public GameObject m_objectCity;

	public void ShowHomeUI(){

		foreach (GameObject btn in m_listButtons) {
			btn.gameObject.SetActive (true);
		}
		m_objectTopBar.SetActive(true);
		if (!m_animateIn) {
		foreach (GameObject btn in m_listButtons) {
			btn.gameObject.SetActive (true);
		}

		m_objectSplashBackground.SetActive (true);

		foreach (GameObject btn in m_listLeftObjects) {
		//btn.gameObject.SetActive (false);
		//btn.transform.localPosition -= new Vector3(30, 0, 0);
			LeanTween.moveLocal (btn, new Vector3(m_currentLeftGroupPosition, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
		}

		foreach (GameObject btn in m_listRightObjects) {
		//btn.gameObject.SetActive (false);
		//btn.transform.localPosition -= new Vector3(30, 0, 0);
			LeanTween.moveLocal (btn, new Vector3(m_currentRightGroupPosition, btn.transform.localPosition.y, btn.transform.localPosition.z), 0.8f).setEase (LeanTweenType.easeOutQuint);
		}

		m_objectSplashBackground.SetActive (true);
		//m_objectWatchVideo.SetActive (false);
		m_animateIn = true;
		}

		

		m_eState = GAME_STATE.START;
	}

	int GetCameraLevel()
	{
		int maxLevel = Mathf.FloorToInt ((m_currentLevel * 1.0f) / 20f);
		if (maxLevel >= 2)
			maxLevel = 2;
		if (m_currentLevel >= 100) {
			maxLevel = 3;
		}
		return maxLevel;
	}

	void UpdateCamera()
	{
		if (m_currentMode == 0 || m_currentMode == 7) {
			//DebugSpawnAll ();
			float orthoSizeLimit = m_listCameraSize [GetCameraLevel ()];
			float heightLimit = m_listCameraHeight [GetCameraLevel ()];
			if (orthoSizeLimit >= m_listCameraSize [m_listCameraSize.Count - 1])
				orthoSizeLimit = m_listCameraSize [m_listCameraSize.Count - 1];
			if (heightLimit >= m_listCameraHeight [m_listCameraHeight.Count - 1])
				heightLimit = m_listCameraHeight [m_listCameraHeight.Count - 1];
			m_objectCamera.GetComponent<Camera> ().orthographicSize = orthoSizeLimit;
			m_objectCamera.transform.localPosition = new Vector3 (m_objectCamera.transform.localPosition.x, heightLimit, m_objectCamera.transform.localPosition.z);
			m_objectFloorObject.transform.position = new Vector3 (m_objectCamera.transform.position.x,
				m_objectFloorObject.transform.position.y, m_objectFloorObject.transform.position.z);
		} 
		else if (m_currentMode == 1 || m_currentMode == 11) 
		{
			m_objectCamera.GetComponent<Camera> ().orthographicSize = 14.36f;
			LeanTween.cancel (m_objectCamera);
			m_objectCamera.transform.localPosition = new Vector3 (-1.8f, 0, m_objectCamera.transform.localPosition.z);

		} 
	else if(m_currentMode == 13)
	{
		m_objectCamera.GetComponent<Camera> ().orthographicSize = 20.36f;
		LeanTween.cancel (m_objectCamera);
		m_objectCamera.transform.localPosition = new Vector3 (-1.8f, 0, m_objectCamera.transform.localPosition.z);

	}
	else if (m_currentMode == 2 || m_currentMode == 9) {
			//float heightLimit2 = m_listCameraHeight [GetCameraLevel ()];
			//if (heightLimit2 >= m_listCameraHeight [3])
			//	heightLimit2 = m_listCameraHeight [3];
			m_objectCamera.GetComponent<Camera> ().orthographicSize = 28.4f;
			m_objectCamera.transform.localPosition = new Vector3 (m_objectCamera.transform.localPosition.x, m_listCameraHeight [2], m_objectCamera.transform.localPosition.z);
			m_objectFloorObject.transform.position = new Vector3 (m_objectCamera.transform.position.x,
				m_objectFloorObject.transform.position.y, m_objectFloorObject.transform.position.z);
		} 
		else if (m_currentMode == 10) 
		{
			m_objectCamera.GetComponent<Camera> ().orthographicSize = 40.4f;
			m_objectCamera.transform.localPosition = new Vector3 (m_objectCamera.transform.localPosition.x, m_listCameraHeight [3], m_objectCamera.transform.localPosition.z);
			m_objectFloorObject.transform.position = new Vector3 (m_objectCamera.transform.position.x,
			m_objectFloorObject.transform.position.y, m_objectFloorObject.transform.position.z);
//			m_objectCamera.GetComponent<Camera> ().orthographicSize = 40f;
//			m_objectCamera.transform.localPosition = new Vector3 (m_objectCamera.transform.localPosition.x, 10f, m_objectCamera.transform.localPosition.z);
//			m_objectFloorObject.transform.position = new Vector3 (m_objectCamera.transform.position.x,
//			m_objectFloorObject.transform.position.y, m_objectFloorObject.transform.position.z);
		}
	else if(m_currentMode == 12)
	{
		m_objectCamera.GetComponent<Camera> ().orthographicSize = 40.4f;
		m_objectCamera.transform.localPosition = new Vector3 (m_objectCamera.transform.localPosition.x, 16, m_objectCamera.transform.localPosition.z);
		m_objectFloorObject.transform.position = new Vector3 (m_objectCamera.transform.position.x,
		m_objectFloorObject.transform.position.y, m_objectFloorObject.transform.position.z);
	}
	else if (m_currentMode == 3 ) 
	{
			int cameraLevel = GetCameraLevel ();
			if (cameraLevel >= 0)
				cameraLevel = 0;
			float orthoSizeLimit = m_listCameraSize [cameraLevel];
			float heightLimit = m_listCameraHeight [cameraLevel];
			if (orthoSizeLimit >= m_listCameraSize [2])
				orthoSizeLimit = m_listCameraSize [2];
			if (heightLimit >= m_listCameraHeight [2])
				heightLimit = m_listCameraHeight [2];
			m_objectCamera.GetComponent<Camera> ().orthographicSize = orthoSizeLimit;
			m_objectCamera.transform.localPosition = new Vector3 (m_objectCamera.transform.localPosition.x, heightLimit, m_objectCamera.transform.localPosition.z);
			m_objectFloorObject.transform.position = new Vector3 (m_objectCamera.transform.position.x,
			m_objectFloorObject.transform.position.y, m_objectFloorObject.transform.position.z);
		} else if (m_currentMode == 4) {
			//DebugSpawnAll ();
			float orthoSizeLimit = m_listCameraSize [GetCameraLevel ()];
			float heightLimit = m_listCameraHeight [GetCameraLevel ()];
			if (orthoSizeLimit >= m_listCameraSize [m_listCameraSize.Count-1])
				orthoSizeLimit = m_listCameraSize [m_listCameraSize.Count-1];
			if (heightLimit >= m_listCameraHeight [m_listCameraHeight.Count-1])
				heightLimit = m_listCameraHeight [m_listCameraHeight.Count-1];
			m_objectCamera.GetComponent<Camera> ().orthographicSize = orthoSizeLimit;
			m_objectCamera.transform.localPosition = new Vector3 (m_objectCamera.transform.localPosition.x, heightLimit, m_objectCamera.transform.localPosition.z);
			m_objectFloorObject.transform.position = new Vector3 (m_objectCamera.transform.position.x,
				m_objectFloorObject.transform.position.y, m_objectFloorObject.transform.position.z);
		}  else if (m_currentMode == 5) {
			m_objectCamera.GetComponent<Camera> ().orthographicSize = 14.36f;
			LeanTween.cancel (m_objectCamera);
			m_objectCamera.transform.localPosition = new Vector3 (-1.8f, 0, m_objectCamera.transform.localPosition.z);
		}  else if (m_currentMode == 6) {
			m_objectCamera.GetComponent<Camera> ().orthographicSize = 25.4f;
			LeanTween.cancel (m_objectCamera);
			m_objectCamera.transform.localPosition = new Vector3 (-0.8f, 6.69f, m_objectCamera.transform.localPosition.z);
		}  else if (m_currentMode == 8) {
			m_objectCamera.GetComponent<Camera> ().orthographicSize = 50.51f;
			LeanTween.cancel (m_objectCamera);
			m_objectCamera.transform.localPosition = new Vector3 (4.94f, 19f, m_objectCamera.transform.localPosition.z);
		} 

		
	}

	float DEFAULT_NEAR = 7f;
	float DEFAULT_MEDIUM = 10f;
	float DEFAULT_HIGH = 16f;

	public List<float> m_listCameraSize;
	public List<float> m_listCameraHeight;

	void DebugSpawnAll()
	{
		ZObjectMgr.Instance.ResetAll ();

		// Current Pole
		int x = 0;
		float currentWidth = 0;
		foreach (Vector3 objVec in m_listPlatformStats) {
			GameObject currentPlatform;
			Vector3 platformStat = objVec;
			currentWidth += objVec.x;
			currentPlatform = SpawnObject (Mathf.FloorToInt (platformStat.z), new Vector2 (currentWidth, platformStat.y));
			currentPlatform.GetComponent<PlatformObject> ().currentNumber = x;
			if (currentPlatform.GetComponent<PlatformObject> ().isHeightLock) 
			{
				currentPlatform.transform.localPosition = new Vector3 (currentPlatform.transform.localPosition.x, 
					currentPlatform.GetComponent<PlatformObject> ().defaultHeight,
					0);
			}
			x++;
		}
	}

	Vector3 GetPlatform(int level)
	{
		if (m_currentMode == 10) 
		{
			if (level >= 100) 
			{
				CityModeLevel curLevel = m_listCityStats [(level + 60) % m_listCityStats.Count];
				m_objectWindManager.GetComponent<WindManager>().SetUpLevel (curLevel.direction, curLevel.windIntensity);
				return new Vector3 (curLevel.x, curLevel.y, (int)curLevel.obj);
			} 
			else 
			{
				CityModeLevel curLevel = m_listCityStats [level  % m_listCityStats.Count];
				m_objectWindManager.GetComponent<WindManager>().SetUpLevel (curLevel.direction, curLevel.windIntensity);
				return new Vector3 (curLevel.x, curLevel.y, (int)curLevel.obj);
			}
			


		
//			if (level >= 299) 
//			{
//				return m_listPlatformStats [m_listPlatformStatsControlledRandom [(level - 299) % m_listPlatformStatsControlledRandom.Count]];
//			} 
//			else 
//			{
//				return m_listPlatformCityStats [level % m_listPlatformCityStats.Count];
//			}
		}
		else if (m_currentMode == 3) 
		{
			// Controlled Random after 199
			//if (level >= 3) {
			//	return m_listPlatformStats [m_listPlatformStatsControlledRandomRelax [(level - 3) % m_listPlatformStatsControlledRandomRelax.Count]];
			Vector3 platformStats;
			if (level >= 299) 
			{
				platformStats = m_listPlatformStats [m_listPlatformStatsControlledRandom [(level - 299) % m_listPlatformStatsControlledRandom.Count]];
			}
			else if (level >= 2) 
			{
				platformStats = m_listPlatformStats [m_listPlatformStatsControlledRandomRelax [(level - 2) % m_listPlatformStatsControlledRandomRelax.Count]];
			} 
			else 
			{
				platformStats = m_listPlatformStats [level];
			}

			if (platformStats.x > 17) 
			{
				platformStats = new Vector3 (17, platformStats.y, platformStats.z);
			} 
			if (platformStats.y > 8) 
			{
				platformStats = new Vector3 (platformStats.x, 8, platformStats.z);
			} 

			return platformStats;
		} 
		else 
		{
			if (level >= 299) 
			{
				return m_listPlatformStats [m_listPlatformStatsControlledRandom [(level - 299) % m_listPlatformStatsControlledRandom.Count]];
			} 
			else 
			{
				return m_listPlatformStats [level];
			}
		}
	}

	GameObject m_targetPlatform;

	GameObject ResetPlatforms()
	{
		int x = 0;
		//foreach (PlatformObject obj in m_listPlatforms) {
		//	obj.gameObject.SetActive (false);
	//
		//}

		float currentWidth = 0;
		float widthToAdd = 0;
		for (int y = 0; y < m_currentLevel; y++) 
		{
			currentWidth += GetPlatform(y).x;
		}

		ZObjectMgr.Instance.ResetAll ();

		// Current Pole
		GameObject currentPlatform;
		Vector3 platformStat = GetPlatform(m_currentLevel);
		currentPlatform = SpawnObject (Mathf.FloorToInt(platformStat.z), new Vector2 (currentWidth, platformStat.y));
		currentPlatform.GetComponent<PlatformObject> ().currentNumber = m_currentLevel;

		if (currentPlatform.GetComponent<PlatformObject> ().isHeightLock) 
		{
			currentPlatform.transform.localPosition = new Vector3 (currentPlatform.transform.localPosition.x, 
			currentPlatform.GetComponent<PlatformObject> ().defaultHeight,
			0);
		}
		// Next Pole
		
		platformStat = GetPlatform(m_currentLevel+1);
		GameObject currentPlatform2 = null;
		
//		if (m_currentMode == 10) 
//		{
//			currentPlatform2 = SpawnObject (Mathf.FloorToInt (platformStat.z), new Vector3 (currentWidth + platformStat.x, platformStat.y, 0));
//		} 
//		else 
//		{
			currentPlatform2 = SpawnObject (Mathf.FloorToInt (platformStat.z), new Vector3 (currentWidth + GetPlatform (m_currentLevel).x, platformStat.y, 0));
//		}
		currentPlatform2.GetComponent<PlatformObject> ().currentNumber = m_currentLevel+1;
		currentPlatform2.GetComponent<PlatformObject> ().currentNumber = m_currentLevel+1;
		if (currentPlatform2.GetComponent<PlatformObject> ().isHeightLock) 
		{
			currentPlatform2.transform.localPosition = new Vector3 (currentPlatform2.transform.localPosition.x, 
				currentPlatform2.GetComponent<PlatformObject> ().defaultHeight,
				0);
		}

		// To set the appropriate wind property
//		if (m_currentMode == 10) 
//		{
//			GetPlatform (m_currentLevel + 1);
//		}
		m_targetPlatform = currentPlatform2;

		if (currentPlatform.GetComponent<PlatformObject> ().isMoving == MOVESTATES.MOVELEFTRIGHT) {
			LeanTween.cancel (currentPlatform);
			//LeanTween.moveLocalX(currentPlatform, currentPlatform.transform.localPosition.x - 8,Random.Range(3f,5f)).setLoopPingPong();
			m_objectBottle.transform.position = currentPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position;
		}
		if (currentPlatform2.GetComponent<PlatformObject> ().isMoving == MOVESTATES.MOVELEFTRIGHT) {
			LeanTween.cancel (currentPlatform2);
			LeanTween.moveLocalX(currentPlatform2, currentPlatform2.transform.localPosition.x - 8,Random.Range(2f,5f)).setLoopPingPong();

		}
		if (currentPlatform.GetComponent<PlatformObject> ().isMoving == MOVESTATES.MOVEUPDOWN) {
			LeanTween.cancel (currentPlatform);
			//LeanTween.moveLocalX(currentPlatform, currentPlatform.transform.localPosition.x - 8,Random.Range(3f,5f)).setLoopPingPong();
			m_objectBottle.transform.position = currentPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position;
		}
		if (currentPlatform2.GetComponent<PlatformObject> ().isMoving == MOVESTATES.MOVEUPDOWN) {
			LeanTween.cancel (currentPlatform2);
			LeanTween.moveLocalY(currentPlatform2, currentPlatform2.transform.localPosition.y - 8,Random.Range(2f,5f)).setLoopPingPong();
		}
		//m_listPlatforms [m_currentLevel].gameObject.SetActive (true);
		//m_listPlatforms [m_currentLevel+1].gameObject.SetActive (true);
		return currentPlatform;
	}

	GameObject ResetPlatformX()
	{
		float currentHeight = 0;
		for (int idx = 0; idx <= m_currentLevel; idx++) 
		{
			currentHeight += 8;
		}

		ZObjectMgr.Instance.ResetAll ();

		// Current Pole
		GameObject currentPlatform;
		Vector3 platformStat = GetPlatform(m_currentLevel);
		currentPlatform = SpawnObject (Mathf.FloorToInt(platformStat.z), new Vector2 (platformStat.x, currentHeight));
		currentPlatform.GetComponent<PlatformObject> ().currentNumber = m_currentLevel;

//		if (currentPlatform.GetComponent<PlatformObject> ().isHeightLock) 
//		{
//			currentPlatform.transform.localPosition = new Vector3 (currentPlatform.transform.localPosition.x, 
//			currentPlatform.GetComponent<PlatformObject> ().defaultHeight, 0);
//		}
		// Next Pole

		platformStat = GetPlatform(m_currentLevel+1);
		GameObject currentPlatform2 = SpawnObject (Mathf.FloorToInt (platformStat.z), new Vector3 ( platformStat.x, currentHeight + GetPlatform(m_currentLevel).y , 0));
		currentPlatform2.GetComponent<PlatformObject> ().currentNumber = m_currentLevel + 1;
		currentPlatform2.GetComponent<PlatformObject> ().currentNumber = m_currentLevel + 1;

		if (currentPlatform2.GetComponent<PlatformObject> ().isHeightLock) 
		{
			currentPlatform2.transform.localPosition = new Vector3 (currentPlatform2.transform.localPosition.x, 
			currentPlatform2.GetComponent<PlatformObject> ().defaultHeight,0);
		}
		m_targetPlatform = currentPlatform2;

		if (currentPlatform.GetComponent<PlatformObject> ().isMoving == MOVESTATES.MOVELEFTRIGHT) 
		{
			LeanTween.cancel (currentPlatform);
			m_objectBottle.transform.position = currentPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position;
		}
		if (currentPlatform2.GetComponent<PlatformObject> ().isMoving == MOVESTATES.MOVELEFTRIGHT) 
		{
			LeanTween.cancel (currentPlatform2);
			LeanTween.moveLocalX(currentPlatform2, currentPlatform2.transform.localPosition.x - 8,Random.Range(2f,5f)).setLoopPingPong();
		}
		if (currentPlatform.GetComponent<PlatformObject> ().isMoving == MOVESTATES.MOVEUPDOWN) 
		{
			LeanTween.cancel (currentPlatform);
			m_objectBottle.transform.position = currentPlatform.GetComponent<PlatformObject> ().spawnArea.transform.position;
		}
		if (currentPlatform2.GetComponent<PlatformObject> ().isMoving == MOVESTATES.MOVEUPDOWN) 
		{
			LeanTween.cancel (currentPlatform2);
			LeanTween.moveLocalY(currentPlatform2, currentPlatform2.transform.localPosition.y - 8,Random.Range(2f,5f)).setLoopPingPong();
		}

		return currentPlatform;
	}

	float m_currentPlatformWidth = 0;
	int m_currentPlatformIndex = 0;
	GameObject SpawnPlatforms(bool isInitial = false)
	{
		int x = 0;
		//foreach (PlatformObject obj in m_listPlatforms) {
		//	obj.gameObject.SetActive (false);
		//
		//}
		//float currentWidth = 0;
		//float widthToAdd = 0;
		//for (int y = 0; y < m_currentLevel; y++) {
		//	currentWidth += m_listPlatformStats [y % m_listPlatformStats.Count].x;
		//}

		//ZObjectMgr.Instance.ResetAll ();


		// Current Pole
		int randomLevel = Random.Range (0, 100);
		GameObject currentPlatform;
		Vector3 platformStat = m_listPlatformStats [randomLevel];

		if (!isInitial) 
		{
			m_currentPlatformWidth += platformStat.x;
		}

		currentPlatform = SpawnObject (Mathf.FloorToInt(platformStat.z), new Vector2 (m_currentPlatformWidth, platformStat.y));
		currentPlatform.GetComponent<PlatformObject> ().currentNumber = m_currentPlatformIndex;
		if (currentPlatform.GetComponent<PlatformObject> ().isHeightLock) 
		{
			currentPlatform.transform.localPosition = new Vector3 (currentPlatform.transform.localPosition.x, 
			currentPlatform.GetComponent<PlatformObject> ().defaultHeight,
			0);
		}

		/*if (currentPlatform.GetComponent<PlatformObject> ().isMoving == MOVESTATES.MOVELEFTRIGHT) {
			LeanTween.cancel (currentPlatform);
			LeanTween.moveLocalX(currentPlatform, currentPlatform.transform.localPosition.x - 5,2f).setLoopPingPong();
		}*/
		// Next Pole

		/*platformStat = m_listPlatformStats [m_currentLevel+1];
		GameObject currentPlatform2 = SpawnObject (Mathf.FloorToInt (platformStat.z), new Vector3 (currentWidth + m_listPlatformStats [m_currentLevel].x, platformStat.y, 0));
		currentPlatform2.GetComponent<PlatformObject> ().currentNumber = m_currentLevel+1;
		currentPlatform2.GetComponent<PlatformObject> ().currentNumber = m_currentLevel+1;
		if (currentPlatform2.GetComponent<PlatformObject> ().isHeightLock) 
		{
			currentPlatform2.transform.localPosition = new Vector3 (currentPlatform2.transform.localPosition.x, 
			currentPlatform2.GetComponent<PlatformObject> ().defaultHeight,
			0);
		}
		m_targetPlatform = currentPlatform2;
		//m_listPlatforms [m_currentLevel].gameObject.SetActive (true);
		//m_listPlatforms [m_currentLevel+1].gameObject.SetActive (true);*/
		m_currentPlatformIndex++;

		return currentPlatform;
	}

	GameObject SpawnObject(int index, Vector3 position)
	{
		GameObject platformObject = m_listPlatformPrefabObjects [index];
		return ZObjectMgr.Instance.Spawn2D (platformObject.name, position);
	}

	private void ChangeColor(int p_score)
	{
		if (m_currentMode != 10 &&
			m_currentMode != 12) 
		{
			return;
		}

		int idx = 0;
		if (m_currentMode == 10) 
		{
			idx = (p_score / 10) % 3;
		} 
		else if (m_currentMode == 12) 
		{
			idx = (p_score / 3) % 3;
		}
		Debug.Log ("IDX: " + idx);
		m_objectCamera.GetComponent<Camera> ().backgroundColor = m_cityCameraColor [idx];
		m_directionalLight.GetComponent<Light> ().color = m_cityLightColor [idx];	
	}

	void ContinueLevel()
	{
		ZObjectMgr.Instance.ResetAll ();

		m_listButtons [0].SetActive (true);
		m_currentSpawnCount = 0;
		m_textLevel.text = "Continue";
		m_textScore.text = "" + m_currentScore;

		m_textLevel.color = m_colorTextSuccess;
		m_textScore.color = m_colorTextSuccess;
		m_objectMiss.SetActive (false);
		m_objectSuccess.SetActive (false);
		//m_objectArrowMock.SetActive (false);
		m_textHit.gameObject.SetActive (true);
		//Debug.LogError ("Setup Level");

		m_objectParticle.SetActive (false);
		m_objectTarget.SetActive (false);

		m_objectTutorial.SetActive (true);
		//MTCharacter.instance.m_currentState = MTCHAR_STATES.IDLE;

		ZAudioMgr.Instance.PlaySFX (m_audioButton);
		ZAnalytics.Instance.SendSkipComplete ();

		m_animateIn = true;
		m_eState = GAME_STATE.START;
		//SpawnBall ();
	}

	void ResetScore()
	{
		m_textLevel.text = "Level";
		m_currentScore = m_currentLevel;
		if (m_currentMode == 9) {
			m_textScore.text = "" + PlayerPrefs.GetInt (ZGameMgr.instance.m_listLeaderboard [m_currentMode].saveName);
		} else {
			m_textScore.text = "" + m_currentScore;
		}
	}

	void ShootArrow()
	{
		/*return;
		GameObject obj = ZObjectMgr.Instance.Spawn2D (m_prefabArrow.name, m_objectCharHead.transform.position - new Vector3(0.1f,0.6f, 0));
		obj.GetComponent<Rigidbody2D> ().isKinematic = false;
		obj.transform.localScale = new Vector3 (0.6f, 0.6f, 0.6f);
		obj.transform.eulerAngles = m_objectCharHead.transform.eulerAngles + new Vector3(0, 0, -90);
		obj.GetComponent<Rigidbody2D> ().AddForce (obj.transform.TransformDirection(new Vector2(0, 1600)));

		obj.transform.parent = m_objectRootArrows.transform;

		obj.GetComponent<BoxCollider2D> ().enabled = true;

		ZAudioMgr.Instance.PlaySFX (m_audioShoot);*/
	}

	/*public override void OnButtonUp(ZButton button)
	{
		if (button.name == "btn_play") {
		}
		else if (button.name == "btn_game") {
		}
		else if (button.name == "btn_pause") {
		}
		else if (button.name == "btn_showad") {
		}
		else if (button.name == "btn_showadvideo") {
		}
	}
	
	public override void OnButtonDown(ZButton button)
	{
		if (button.name == "btn_game") {
		}
	}*/
}
