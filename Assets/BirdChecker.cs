﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class BirdChecker : MonoBehaviour
{
	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.GetComponent<Bird> ()) {
			if (!coll.gameObject.GetComponent<Bird> ().m_isAlive)
				return;

			coll.gameObject.GetComponent<Bird> ().Reset ();
			coll.gameObject.SetActive (false);

			GameScene.instance.Die ();
		}
	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.GetComponent<Bird> ()) {
			if (!coll.gameObject.GetComponent<Bird> ().m_isAlive)
				return;

			coll.gameObject.GetComponent<Bird> ().Reset ();
			coll.gameObject.SetActive (false);

			GameScene.instance.Die ();
		}
	}
}
